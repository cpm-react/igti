import { useState, useEffect } from "react";
import { useRouter } from "next/router";

import Swal from "sweetalert2";

import Admin from "layouts/Admin.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import CardHeader from "components/Card/CardHeader.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import CustomSelect from "../../components/CustomSelect/CustomSelect";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";

import {
  GetAtendimentoPorId,
  PutAtendimento,
  DeleteAtendimento,
} from "../../services/Atendimento";
import { PostEvolucao } from "../../services/Evolucao";
import { GetFormularios } from "../../services/Formulario";
import { AtedimentoStatus } from "../enum";
import { Evolucao } from "./Evolucoes";

const EditarAtendimento = () => {
  const router = useRouter();
  const { editar } = router.query;

  const [agendamento, setAgendamento] = useState([]);
  const [cliente, setCliente] = useState([]);
  const [clienteNome, setClienteNome] = useState([]);
  const [formulario, setFormulario] = useState([]);
  const [formularios, setFormularios] = useState([]);
  const [id, setId] = useState([]);
  const [profissional, setProfissional] = useState([]);
  const [profissionalNome, setProfissionalNome] = useState([]);
  const [status, setStatus] = useState([]);
  const [evolucoes, setEvolucoes] = useState([]);

  async function fetchGetAtendimentos() {
    const retorno = await GetAtendimentoPorId(editar);
    if (retorno) {
      setAgendamento(retorno.agendamento);
      setCliente(retorno.cliente);
      setClienteNome(retorno.cliente.nome);
      setId(editar);
      setProfissional(retorno.profissional);
      setProfissionalNome(retorno.profissional.nome);
      setStatus(retorno.status);
      setEvolucoes(retorno.evolucoes);
    }
  }

  async function fetchFormularios() {
    const retorno = await GetFormularios();

    if (retorno) {
      const result = retorno.results.map((form) => ({
        value: form.id,
        label: form.codigo + " - " + form.descricao,
      }));

      setFormularios(result);
    }
  }

  async function salvaAtendimento() {
    PutAtendimento(editar, {
      agendamento,
      cliente: cliente.id,
      id: editar,
      profissional: profissional.id,
      status,
    });

    Swal.fire("Sucesso", "Atualizado com sucesso", "success");
  }

  async function salvaEvolucao() {
    PostEvolucao({
      atendimento: editar,
      formulario,
    });

    Swal.fire("Sucesso", "Atualizado com sucesso", "success");
  }

  useEffect(() => {
    fetchFormularios();
    fetchGetAtendimentos();
  }, []);

  const handleSubmit = (event) => {
    event.preventDefault();

    salvaAtendimento();

    if (formulario) {
      salvaEvolucao();
    }

    fetchGetAtendimentos();
  };

  const voltar = () => {
    router.push("/agenda");
  };

  const excluir = () => {
    Swal.fire({
      title: "Deseja excluir esse Atendimento??",
      showDenyButton: true,
      confirmButtonText: `Sim`,
      denyButtonText: `Não`,
    }).then((result) => {
      if (result.isConfirmed) {
        DeleteAtendimento(editar);
        voltar();
      }
    });
  };

  const formControlProps = {
    fullWidth: true,
  };

  return (
    <>
      <form onSubmit={handleSubmit}>
        <Card>
          <CardHeader color="info">
            <h4>
              {cliente.nome} - {agendamento}
            </h4>
          </CardHeader>
          <CardBody>
            <GridContainer>
              <GridItem xs={12} sm={12} md={3}>
                <CustomInput
                  labelText="Id"
                  id="Id"
                  type="text"
                  value={id}
                  formControlProps={formControlProps}
                  error={false}
                  required
                  disabled
                />
              </GridItem>
              <GridItem xs={12} sm={12} md={3}>
                <CustomInput
                  labelText="Cliente"
                  id="cliente"
                  type="text"
                  value={clienteNome}
                  formControlProps={formControlProps}
                  error={false}
                  required
                  disabled
                />
              </GridItem>
              <GridItem xs={12} sm={12} md={3}>
                <CustomInput
                  labelText="Profissional"
                  id="profissional"
                  type="text"
                  value={profissionalNome}
                  formControlProps={formControlProps}
                  error={false}
                  required
                  disabled
                />
              </GridItem>
              <GridItem xs={12} sm={12} md={3}>
                <CustomSelect
                  labelText="status"
                  id="status"
                  value={status}
                  currencies={AtedimentoStatus()}
                  formControlProps={formControlProps}
                  onChange={(event) => setStatus(event.target.value)}
                  error={false}
                  maxlength="1"
                  required
                />
              </GridItem>
            </GridContainer>

            {status == "ATENDENDO" && (
              <GridContainer>
                <GridItem xs={12} sm={12} md={10}>
                  <CustomSelect
                    labelText="Formulários"
                    id="formularios"
                    value={formulario}
                    currencies={formularios}
                    formControlProps={formControlProps}
                    onChange={(event) => setFormulario(event.target.value)}
                    error={false}
                    maxlength="1"
                    required
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={2} style={{ marginTop: "30px" }}>
                  <Button color="success" type="submit">
                    Incluir
                  </Button>
                </GridItem>
              </GridContainer>
            )}
          </CardBody>
          <CardFooter>
            <Button onClick={voltar}>Voltar</Button>
            <Button color="danger" onClick={excluir}>
              Excluir
            </Button>
            <Button color="success" type="submit">
              Salvar
            </Button>
          </CardFooter>
        </Card>
      </form>

      <Evolucao editar={editar} lista={evolucoes}></Evolucao>
    </>
  );
};

EditarAtendimento.layout = Admin;
export default EditarAtendimento;
