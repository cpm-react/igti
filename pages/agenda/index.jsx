import { useEffect, useState } from "react";
import { useRouter } from "next/router";

// layout for this page
import Admin from "layouts/Admin.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardBody from "components/Card/CardBody.js";
import { Calendar, momentLocalizer } from "react-big-calendar";
import moment from "moment";

import {
  GetAtendimentoPorId,
  GetAtendimentoPorProfissional,
  PostAtendimento,
  PutAtendimento,
} from "../../services/Atendimento";
import { AtedimentoStatusFiltro } from "../enum";
import { GetClientes } from "../../services/Cliente";
import { GetProfissionalDoUsuario } from "../../services/Sessao";
import { GetUsuarioProfissionalPorUsuarioId } from "../../services/UsuarioProfissional";

require("react-big-calendar/lib/css/react-big-calendar.css");
import Swal from "sweetalert2";
moment.locale("pt-BR");

const localizer = momentLocalizer(moment);

const Agenda = () => {
  const [atendimentos, setAtendimentos] = useState([]);
  const [clientes, setClientes] = useState([]);
  const [profissionais, setProfissionais] = useState([]);

  const router = useRouter();

  const montarSelect = (id, lista) => {
    return `<select class="swal2-select" style="display: flex;text-transform: 
    capitalize;" id="${id}"><option value="" disabled selected>${id}</option>${montarOptions(
      lista
    )}</select>`;
  };

  const montarOptions = (retorno) => {
    let mapa = "";
    retorno.results.map((item) => {
      mapa += '<option value="' + item.id + '">' + item.nome + "</option>";
    });

    return mapa;
  };

  async function executaGetAtendimentos() {
    const retorno = await GetAtendimentoPorProfissional(
      GetProfissionalDoUsuario()
    );

    if (retorno && retorno.results) {
      const lista = [];
      retorno.results.map((atendimento) => {
        if (
          atendimento.status === AtedimentoStatusFiltro("AGENDADO")[0].value &&
          atendimento.profissional.id === GetProfissionalDoUsuario()
        ) {
          let start = new Date(atendimento.agendamento);

          lista.push({
            id: atendimento.id,
            title: atendimento.cliente.nome,
            start,
            end: moment(start).add(95, "m").toDate(),
          });
        }
      });
      setAtendimentos(lista);
    }
  }

  async function executaGetAtendimentoPorId(id) {
    const retorno = await GetAtendimentoPorId(id);

    if (retorno) {
      return retorno;
    }
  }

  async function executaGetClientes() {
    const retorno = await GetClientes();

    if (retorno) {
      setClientes(retorno);
    }
  }

  async function executaGetProfissionais() {
    const retorno = await GetUsuarioProfissionalPorUsuarioId();

    if (retorno) {
      setProfissionais(retorno);
    }
  }

  async function salvarAtendimento(atendimento) {
    const retorno = await PostAtendimento(atendimento);
    Swal.fire("Sucesso", "Criado com sucesso", "success").then((result) => {
      if (result.isConfirmed) {
        executaGetAtendimentos();
      }
    });
  }

  async function cancelarAtendimento(id) {
    const filtrado = await executaGetAtendimentoPorId(id);
    setTimeout(() => {
      if (filtrado) {
        filtrado.cliente = filtrado.cliente.id;
        filtrado.profissional = filtrado.profissional.id;
        filtrado.status = AtedimentoStatusFiltro("CANCELADO")[0].value;
        const retorno = PutAtendimento(filtrado.id, filtrado);
        if (retorno) {
          Swal.fire("Sucesso", "Atualizado com sucesso", "success").then(
            (result) => {
              if (result.isConfirmed) {
                executaGetAtendimentos();
              }
            }
          );
        }
      }
    }, 500);
  }

  useEffect(() => {
    executaGetClientes();
    executaGetProfissionais();
    executaGetAtendimentos();
  }, []);

  const gerenciarCliequeNaTabela = ({ start, end }) => {
    Swal.fire({
      title: "Atendimento",
      showCancelButton: true,
      html:
        montarSelect("cliente", clientes) +
        montarSelect("profissional", profissionais),
      focusConfirm: false,
      preConfirm: () => {
        salvarAtendimento({
          agendamento: start,
          cliente: document.getElementById("cliente").value,
          profissional: document.getElementById("profissional").value,
          status: AtedimentoStatusFiltro("AGENDADO")[0].value,
        });
      },
      onOpen: function () {
        document.getElementById("profissional").value =
          GetProfissionalDoUsuario();
      },
    });
  };

  // Função para manegar o evento
  function cliqueEvento(evento) {
    Swal.fire({
      title: evento.title,
      showDenyButton: true,
      showCancelButton: true,
      confirmButtonText: "Iniciar",
      denyButtonText: `Cancelar Atendimento`,
    }).then((result) => {
      if (result.isConfirmed) {
        router.push(`agenda/${evento.id}`);
      } else if (result.isDenied) {
        cancelarAtendimento(evento.id);
      }
    });
  }

  return (
    <Card>
      <CardHeader color="info">
        <h4>Agenda!!</h4>
        <p>Horários disponíveis</p>
      </CardHeader>
      <CardBody>
        <div>
          <Calendar
            selectable={true}
            events={atendimentos}
            step={30}
            showMultiDayTimes={true}
            localizer={localizer}
            style={{ height: 500 }}
            messages={{
              next: "Prox",
              previous: "Ant",
              today: "Hoje",
              month: "Mês",
              week: "Semana",
              day: "Dia",
            }}
            onSelectEvent={(event) => cliqueEvento(event)}
            onSelectSlot={gerenciarCliequeNaTabela}
            scrollToTime={new Date(moment().unix())}
            defaultView={"week"}
          />
        </div>
      </CardBody>
    </Card>
  );
};

Agenda.layout = Admin;
export default Agenda;
