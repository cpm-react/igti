import { useRouter } from "next/router";

import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import CardHeader from "components/Card/CardHeader.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import { CustomSelectMelhor } from "../../components/CustomSelect/CustomSelectMelhor";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Table from "components/Table/Table.js";

import { makeStyles } from "@material-ui/core/styles";
import styles from "assets/jss/nextjs-material-dashboard/views/dashboardStyle.js";

import { PostEvolucaoResposta } from "../../services/EvolucaoResposta";

const formControlProps = {
  fullWidth: true,
};

const Evolucao = (props) => {
  const router = useRouter();
  const useStyles = makeStyles(styles);
  const classes = useStyles();

  const atualizar = () => {
    setTimeout(() => {
      router.reload();
    }, 1000);
  };

  const retornaImput = (item) => {
    return (
      <CustomInput
        labelText={item.pergunta.descricao}
        id={item.id}
        type={item.pergunta.tipo_pergunta}
        formControlProps={formControlProps}
        error={false}
      />
    );
  };

  const retornaSelect = (item) => {
    return (
      <CustomSelectMelhor
        labelText={`${item.pergunta.descricao}`}
        id={item.id}
        formControlProps={formControlProps}
        currencies={item.pergunta.opcoes.map((opcao) => ({
          value: opcao.opcao,
          label: opcao.opcao,
        }))}
        multiple={item.pergunta.tipo_pergunta == "CHECKBOX"}
        required
      />
    );
  };

  const montaPerguntas = (perguntas) => {
    return perguntas.map((item) => {
      return (
        <>
          <GridItem xs={12} sm={12} md={6}>
            {item.pergunta.tipo_pergunta == "RADIO" ||
            item.pergunta.tipo_pergunta == "COMBO" ||
            item.pergunta.tipo_pergunta == "CHECKBOX"
              ? retornaSelect(item)
              : retornaImput(item)}
          </GridItem>
        </>
      );
    });
  };

  const montarTabela = (respostas) => {
    const retorno = respostas.map((resposta) => {
      return [resposta.pergunta_texto, resposta.resposta_texto];
    });

    return (
      <Table
        tableHeaderColor="info"
        tableHead={["Pergunta", "Resposta"]}
        tableData={retorno}
        linkUrl={``}
      />
    );
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    Array.from(event.target.getElementsByClassName("MuiInput-input")).forEach(
      (el) => {
        PostEvolucaoResposta({
          pergunta_texto: document.getElementById(`label_${el.id}`).innerHTML,
          resposta_texto:
            el.tagName == 'DIV'
              ? el.innerHTML
              : el.value,
          evolucao: event.target.id,
        });
      }
    );

    atualizar();
  };

  const listItems = props.lista.map((evolucao) => (
    <form id={evolucao.id} onSubmit={handleSubmit}>
      <GridContainer>
        <GridItem xs={12} sm={12} md={12}>
          <Card>
            <CardHeader color="info">
              <h4 className={classes.cardTitleWhite}>
                {evolucao.formulario.descricao} - {evolucao.criado}
              </h4>
            </CardHeader>
            <CardBody>
              <GridContainer>
                {evolucao.respostas.length == 0
                  ? montaPerguntas(evolucao.formulario.perguntas_do_formulario)
                  : montarTabela(evolucao.respostas)}
              </GridContainer>
            </CardBody>
            <CardFooter>
              {evolucao.respostas.length == 0 && (
                <Button color="success" type="submit">
                  Salvar
                </Button>
              )}
            </CardFooter>
          </Card>
        </GridItem>
      </GridContainer>
    </form>
  ));

  return listItems;
};

export { Evolucao };
