import { useEffect, useState } from "react";
import { useRouter } from "next/router";

import Admin from "layouts/Admin.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import CardHeader from "components/Card/CardHeader.js";
import Table from "components/Table/Table.js";

import { GetCids } from "../../services/Cid";

const Cid = () => {
  const [cids, setCids] = useState([]);
  const router = useRouter();

  useEffect(() => {
    async function fetchGetCids() {
      const retorno = await GetCids();

      if (retorno) {
        const result = retorno.results.map((cid) => [
          cid.id,
          cid.descricao,
          cid.codigo,
        ]);
  
        setCids(result);
      }
    }

    fetchGetCids();
  }, []);

  const novoCid = () => {
    router.push("/cid/novo");
  };

  return (
    <Card>
      <CardHeader color="info">
        <h4>CID!!</h4>
        <p>
          Classificação Estatística Internacional de Doenças e Problemas
          Relacionados com a Saúde
        </p>
      </CardHeader>
      <CardBody>
        <Table
          tableHeaderColor="info"
          tableHead={["ID", "Descrição", "Código"]}
          tableData={cids}
          linkUrl="/cid"
        />
      </CardBody>
      <CardFooter>
        <Button onClick={novoCid}>Novo</Button>
      </CardFooter>
    </Card>
  );
};

Cid.layout = Admin;
export default Cid;
