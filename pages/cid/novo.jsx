import { useEffect } from "react";
import { useState } from "react";
import { useRouter } from "next/router";

import Swal from "sweetalert2";

import Admin from "layouts/Admin.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import CardHeader from "components/Card/CardHeader.js";
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import CustomInput from "components/CustomInput/CustomInput.js";

import { PostCid } from "../../services/Cid";
import { GetSessao } from "../../services/Usuario";

const NovoCid = () => {
  // Ready
  useEffect(() => {
    GetSessao();
  }, []);

  const router = useRouter();

  const [descricao, setDescricao] = useState([]);
  const [codigo, setCodigo] = useState([]);

  const handleSubmit = (event) => {
    event.preventDefault();

    async function salvarCid() {
      const retorno = await PostCid({ descricao, codigo });
      Swal.fire("Sucesso", "Criado com sucesso", "success").then((result) => {
        if (result.isConfirmed) {
          router.push(`/cid/${retorno.id}`);
        }
      });
    }

    salvarCid();
  };

  const formControlProps = {
    fullWidth: true,
  };

  return (
    <form onSubmit={handleSubmit}>
      <Card>
        <CardHeader color="info">
          <h4>Novo CID!!</h4>
          <p>
            Novo Classificação Estatística Internacional de Doenças e Problemas
            Relacionados com a Saúde
          </p>
        </CardHeader>
        <CardBody>
          <GridContainer>
            <GridItem xs={12} sm={12} md={7}>
              <CustomInput
                labelText="Descrição"
                id="descricao"
                type="text"
                value={descricao}
                formControlProps={formControlProps}
                onChange={(event) => setDescricao(event.target.value)}
                error={false}
                maxlength="200"
                required
              />
            </GridItem>
            <GridItem xs={12} sm={12} md={2}>
              <CustomInput
                labelText="Código"
                id="codigo"
                type="text"
                value={codigo}
                formControlProps={formControlProps}
                onChange={(event) => setCodigo(event.target.value)}
                error={false}
                maxlength="15"
                required
              />
            </GridItem>
          </GridContainer>
        </CardBody>
        <CardFooter>
          <Button
            onClick={() => {
              router.push("/cid");
            }}
          >
            Cancelar
          </Button>
          <Button color="success" type="submit">
            Salvar
          </Button>
        </CardFooter>
      </Card>
    </form>
  );
};

NovoCid.layout = Admin;
export default NovoCid;
