import { useState, useEffect } from "react";
import { useRouter } from "next/router";

import Swal from "sweetalert2";

import Admin from "layouts/Admin.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import CardHeader from "components/Card/CardHeader.js";
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import CustomInput from "components/CustomInput/CustomInput.js";

import { GetCidPorId, PutCid, DeleteCid } from "../../services/Cid";

const EditarCid = () => {
  const router = useRouter();
  const { editar } = router.query;

  const [id, setId] = useState([]);
  const [descricao, setDescricao] = useState([]);
  const [codigo, setCodigo] = useState([]);

  useEffect(() => {
    async function fetchGetCids() {
      const retorno = await GetCidPorId(editar);
      if (retorno) {
        setId(editar);
        setDescricao(retorno.descricao);
        setCodigo(retorno.codigo);
      }
    }

    fetchGetCids();
  }, []);

  const handleSubmit = (event) => {
    event.preventDefault();

    async function fetchGetCids() {
      PutCid(editar, { id: editar, descricao, codigo });

      Swal.fire("Sucesso", "Atualizado com sucesso", "success");
    }

    fetchGetCids();
  };

  const voltar = () => {
    router.push("/cid");
  };

  const excluir = () => {
    Swal.fire({
      title: "Deseja excluir esse Cid??",
      showDenyButton: true,
      confirmButtonText: `Sim`,
      denyButtonText: `Não`,
    }).then((result) => {
      if (result.isConfirmed) {
        DeleteCid(editar);
        voltar();
      }
    });
  };

  const formControlProps = {
    fullWidth: true,
  };

  return (
    <form onSubmit={handleSubmit}>
      <Card>
        <CardHeader color="info">
          <h4>Editar Cid!!</h4>
          <p>
            Editar Classificação Estatística Internacional de Doenças e
            Problemas Relacionados com a Saúde
          </p>
        </CardHeader>
        <CardBody>
          <GridContainer>
            <GridItem xs={12} sm={12} md={3}>
              <CustomInput
                labelText="Id"
                id="Id"
                type="text"
                value={id}
                formControlProps={formControlProps}
                error={false}
                required
                disabled
              />
            </GridItem>
            <GridItem xs={12} sm={12} md={6}>
              <CustomInput
                labelText="Descrição"
                id="descricao"
                type="text"
                value={descricao}
                formControlProps={formControlProps}
                onChange={(event) => setDescricao(event.target.value)}
                error={false}
                maxlength="200"
                required
              />
            </GridItem>
            <GridItem xs={12} sm={12} md={2}>
              <CustomInput
                labelText="Código"
                id="codigo"
                type="text"
                value={codigo}
                formControlProps={formControlProps}
                onChange={(event) => setCodigo(event.target.value)}
                error={false}
                maxlength="15"
                required
              />
            </GridItem>
          </GridContainer>
        </CardBody>
        <CardFooter>
          <Button onClick={voltar}>Cancelar</Button>
          <Button color="danger" onClick={excluir}>
            Excluir
          </Button>
          <Button color="success" type="submit">
            Salvar
          </Button>
        </CardFooter>
      </Card>
    </form>
  );
};

EditarCid.layout = Admin;
export default EditarCid;
