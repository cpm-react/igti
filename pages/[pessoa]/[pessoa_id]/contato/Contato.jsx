import { useRouter } from "next/router";

import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import CardHeader from "components/Card/CardHeader.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Table from "components/Table/Table.js";

import { makeStyles } from "@material-ui/core/styles";
import styles from "assets/jss/nextjs-material-dashboard/views/dashboardStyle.js";

import { TipoContatoFiltroLabel } from "../../../enum";

const Contato = (props) => {
  const router = useRouter();
  const useStyles = makeStyles(styles);
  const classes = useStyles();

  const novoContato = () => {
    router.push(`/${props.pessoa}/${props.editar}/contato/novo`);
  };

  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={12}>
        <Card>
          <CardHeader color="info">
            <h4 className={classes.cardTitleWhite}>Contatos</h4>
          </CardHeader>
          <CardBody>
            <Table
              tableHeaderColor="info"
              tableHead={["Id", "Tipo", "Título", "Valor"]}
              tableData={props.lista}
              linkUrl={`/${props.pessoa}/${props.editar}/contato`}
            />
          </CardBody>
          <CardFooter>
            <Button onClick={novoContato}>Novo Contato</Button>
          </CardFooter>
        </Card>
      </GridItem>
    </GridContainer>
  );
};

const ContatoConverter = (lista) => {
  return lista.map((contato) => [
    contato.id,
    TipoContatoFiltroLabel(contato.tipo),
    contato.titulo,
    contato.valor,
  ]);
};

export { Contato, ContatoConverter };
