import { useState, useEffect } from "react";
import { useRouter } from "next/router";

import Swal from "sweetalert2";

import Admin from "layouts/Admin.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import CardHeader from "components/Card/CardHeader.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import CustomSelect from "../../../../components/CustomSelect/CustomSelect";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";

import {
  PutContato,
  GetContatoPorId,
  DeleteContato,
} from "../../../../services/Contato";
import { TipoContato } from "../../../enum";

const NovoContato = () => {
  const router = useRouter();
  const { pessoa, pessoa_id, editar } = router.query;

  const [id, setId] = useState([]);
  const [tipo, setTipo] = useState([]);
  const [titulo, setTitulo] = useState([]);
  const [valor, setValor] = useState([]);

  // Ready
  useEffect(() => {
    async function fetchGetUsuarios() {
      const retorno = await GetContatoPorId(editar);
      if (retorno) {
        setId(editar);
        setTipo(retorno.tipo);
        setTitulo(retorno.titulo);
        setValor(retorno.valor);
      }
    }

    fetchGetUsuarios();
  }, []);

  const handleSubmit = (event) => {
    event.preventDefault();

    async function fetchGetContatos() {
      const retorno = await PutContato(editar, {
        id: editar,
        tipo,
        titulo,
        valor,
        pessoa: pessoa_id,
      });
      if (retorno) {
        Swal.fire("Sucesso", "Atualizado com sucesso", "success").then(
          (result) => {
            if (result.isConfirmed) {
              voltar();
            }
          }
        );
      }
    }

    fetchGetContatos();
  };

  const formControlProps = {
    fullWidth: true,
  };

  const voltar = () => {
    router.push(`/${pessoa}/${pessoa_id}`);
  };

  const excluir = () => {
    Swal.fire({
      title: "Deseja excluir esse Contato??",
      showDenyButton: true,
      confirmButtonText: `Sim`,
      denyButtonText: `Não`,
    }).then((result) => {
      if (result.isConfirmed) {
        DeleteContato(editar);
        voltar();
      }
    });
  };

  return (
    <form onSubmit={handleSubmit}>
      <Card>
        <CardHeader color="info">
          <h4>Editar Contato!!</h4>
          <p>Editar Contato</p>
        </CardHeader>
        <CardBody>
          <GridContainer>
            <GridItem xs={12} sm={12} md={6}>
              <CustomInput
                labelText="Id"
                id="Id"
                type="text"
                value={id}
                formControlProps={formControlProps}
                error={false}
                required
                disabled
              />
            </GridItem>
            <GridItem xs={12} sm={12} md={6}>
              <CustomSelect
                labelText="Tipo"
                id="tipo"
                value={tipo}
                currencies={TipoContato()}
                formControlProps={formControlProps}
                onChange={(event) => setTipo(event.target.value)}
                error={false}
                required
              />
            </GridItem>
          </GridContainer>
          <GridContainer>
            <GridItem xs={12} sm={12} md={6}>
              <CustomInput
                labelText="Título"
                id="titulo"
                type="text"
                value={titulo}
                formControlProps={formControlProps}
                onChange={(event) => setTitulo(event.target.value)}
                error={false}
                maxlength="50"
                required
              />
            </GridItem>
            <GridItem xs={12} sm={12} md={6}>
              <CustomInput
                labelText="Valor"
                id="valor"
                type="text"
                value={valor}
                formControlProps={formControlProps}
                onChange={(event) => setValor(event.target.value)}
                error={false}
                maxlength="50"
                required
              />
            </GridItem>
          </GridContainer>
        </CardBody>
        <CardFooter>
          <Button onClick={voltar}>Cancelar</Button>
          <Button color="danger" onClick={excluir}>
            Excluir
          </Button>
          <Button color="success" type="submit">
            Salvar
          </Button>
        </CardFooter>
      </Card>
    </form>
  );
};

NovoContato.layout = Admin;
export default NovoContato;
