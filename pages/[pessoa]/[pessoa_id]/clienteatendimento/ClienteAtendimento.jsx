import { useRouter } from "next/router";

import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import CardHeader from "components/Card/CardHeader.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Table from "components/Table/Table.js";

import { makeStyles } from "@material-ui/core/styles";
import styles from "assets/jss/nextjs-material-dashboard/views/dashboardStyle.js";

const ClienteAtendimento = (props) => {
  const router = useRouter();
  const useStyles = makeStyles(styles);
  const classes = useStyles();

  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={12}>
        <Card>
          <CardHeader color="info">
            <h4 className={classes.cardTitleWhite}>Atendimentos</h4>
          </CardHeader>
          <CardBody>
            <Table
              tableHeaderColor="info"
              tableHead={["ID", "Agendamento", "Status"]}
              tableData={props.lista}
              linkUrl={`/agenda`}
            />
          </CardBody>
          <CardFooter></CardFooter>
        </Card>
      </GridItem>
    </GridContainer>
  );
};

const ClienteAtendimentoConverter = (lista) => {
  return lista.map((cliente_atendimento) => [
    cliente_atendimento.id,
    cliente_atendimento.agendamento,
    cliente_atendimento.status,
  ]);
};

export { ClienteAtendimento, ClienteAtendimentoConverter };
