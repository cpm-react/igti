import { useRouter } from "next/router";

import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import CardHeader from "components/Card/CardHeader.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Table from "components/Table/Table.js";

import { makeStyles } from "@material-ui/core/styles";
import styles from "assets/jss/nextjs-material-dashboard/views/dashboardStyle.js";

const UsuarioPapel = (props) => {
  const router = useRouter();
  const useStyles = makeStyles(styles);
  const classes = useStyles();

  const novoUsuarioPapel = () => {
    router.push(`/${props.pessoa}/${props.editar}/usuariopapel/novo`);
  };

  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={12}>
        <Card>
          <CardHeader color="info">
            <h4 className={classes.cardTitleWhite}>Papéis</h4>
          </CardHeader>
          <CardBody>
            <Table
              tableHeaderColor="info"
              tableHead={["ID", "Descrição", "Código"]}
              tableData={props.lista}
              linkUrl={`/${props.pessoa}/${props.editar}/usuariopapel`}
            />
          </CardBody>
          <CardFooter>
            <Button onClick={novoUsuarioPapel}>Incluir Papel</Button>
          </CardFooter>
        </Card>
      </GridItem>
    </GridContainer>
  );
};

const UsuarioPapelConverter = async (lista) => {
  return lista.map((usuario_papel) => [
    usuario_papel.id,
    usuario_papel.papel.descricao,
    usuario_papel.papel.codigo,
  ]);
};

export { UsuarioPapel, UsuarioPapelConverter };
