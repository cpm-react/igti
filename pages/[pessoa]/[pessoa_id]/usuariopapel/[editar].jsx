import { useState, useEffect } from "react";
import { useRouter } from "next/router";

import Swal from "sweetalert2";

import Admin from "layouts/Admin.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import CardHeader from "components/Card/CardHeader.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import CustomSelect from "../../../../components/CustomSelect/CustomSelect";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";

import {
  PutUsuarioPapel,
  GetUsuarioPapelPorId,
  DeleteUsuarioPapel,
} from "../../../../services/UsuarioPapel";
import { GetPapels } from "../../../../services/Papel";

const NovoUsuarioPapel = () => {
  const router = useRouter();
  const { pessoa, pessoa_id, editar } = router.query;

  const [id, setId] = useState([]);
  const [papel, setPapel] = useState([]);
  const [papeis, setPapeis] = useState([]);

  // Ready
  useEffect(() => {
    async function fetchGetUsuarios() {
      const retorno = await GetUsuarioPapelPorId(editar);
      if (retorno) {
        setId(editar);
        setPapel(retorno.papel);
      }
    }
    async function fetchPapeis() {
      const retorno = await GetPapels();

      if (retorno) {
        const result = retorno.results.map((papel) => ({
          value: papel.id,
          label: papel.codigo,
        }));

        setPapeis(result);
      }
    }

    fetchGetUsuarios();
    fetchPapeis();
  }, []);

  const handleSubmit = (event) => {
    event.preventDefault();

    async function fetchGetUsuarioPapeis() {
      const retorno = await PutUsuarioPapel(editar, {
        id: editar,
        papel,
        pessoa: pessoa_id,
      });
      if (retorno) {
        Swal.fire("Sucesso", "Atualizado com sucesso", "success").then(
          (result) => {
            if (result.isConfirmed) {
              voltar();
            }
          }
        );
      }
    }

    fetchGetUsuarioPapeis();
  };

  const formControlProps = {
    fullWidth: true,
  };

  const voltar = () => {
    router.push(`/${pessoa}/${pessoa_id}`);
  };

  const excluir = () => {
    Swal.fire({
      title: "Deseja excluir esse papel do usuario?",
      showDenyButton: true,
      confirmButtonText: `Sim`,
      denyButtonText: `Não`,
    }).then((result) => {
      if (result.isConfirmed) {
        DeleteUsuarioPapel(editar);
        voltar();
      }
    });
  };

  return (
    <form onSubmit={handleSubmit}>
      <Card>
        <CardHeader color="info">
          <h4>Editar papel do usuario!!</h4>
          <p>Editar papel do usuario</p>
        </CardHeader>
        <CardBody>
          <GridContainer>
            <GridItem xs={12} sm={12} md={6}>
              <CustomInput
                labelText="Id"
                id="Id"
                type="text"
                value={id}
                formControlProps={formControlProps}
                error={false}
                required
                disabled
              />
            </GridItem>
            <GridItem xs={12} sm={12} md={6}>
              <CustomSelect
                labelText="Papel"
                id="papel"
                value={papel}
                currencies={papeis}
                formControlProps={formControlProps}
                onChange={(event) => setPapel(event.target.value)}
                error={false}
                required
              />
            </GridItem>
          </GridContainer>
        </CardBody>
        <CardFooter>
          <Button onClick={voltar}>Cancelar</Button>
          <Button color="danger" onClick={excluir}>
            Excluir
          </Button>
          <Button color="success" type="submit">
            Salvar
          </Button>
        </CardFooter>
      </Card>
    </form>
  );
};

NovoUsuarioPapel.layout = Admin;
export default NovoUsuarioPapel;
