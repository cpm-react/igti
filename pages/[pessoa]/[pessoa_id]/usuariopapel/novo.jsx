import { useState, useEffect } from "react";
import { useRouter } from "next/router";

import Swal from "sweetalert2";

import Admin from "layouts/Admin.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import CardHeader from "components/Card/CardHeader.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import CustomSelect from "../../../../components/CustomSelect/CustomSelect";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";

import { PostUsuarioPapel } from "../../../../services/UsuarioPapel";

import { GetPapels } from "../../../../services/Papel";

const NovoUsuarioPapel = () => {
  // Ready
  useEffect(() => {
    async function fetchPapeis() {
      const retorno = await GetPapels();

      if (retorno) {
        const result = retorno.results.map((papel) => ({
          value: papel.id,
          label: papel.codigo,
        }));

        setPapeis(result);
      }
    }

    fetchPapeis();
  }, []);

  const router = useRouter();
  const { pessoa, pessoa_id } = router.query;

  const [papel, setPapel] = useState([]);
  const [papeis, setPapeis] = useState([]);

  const handleSubmit = (event) => {
    event.preventDefault();

    async function saveUsuarioPapels() {
      const retorno = await PostUsuarioPapel({
        papel,
        usuario: pessoa_id,
      });
      if (retorno) {
        Swal.fire("Sucesso", "Criado com sucesso", "success").then((result) => {
          if (result.isConfirmed) {
            voltar();
          }
        });
      }
    }

    saveUsuarioPapels();
  };

  const voltar = () => {
    router.push(`/${pessoa}/${pessoa_id}`);
  };

  const formControlProps = {
    fullWidth: true,
  };

  return (
    <form onSubmit={handleSubmit}>
      <Card>
        <CardHeader color="info">
          <h4>Novo UsuarioPapel!!</h4>
          <p>Novo UsuarioPapel</p>
        </CardHeader>
        <CardBody>
          <GridContainer>
            <GridItem xs={12} sm={12} md={2}>
              <CustomSelect
                labelText="Papel"
                id="papel"
                currencies={papeis}
                formControlProps={formControlProps}
                onChange={(event) => setPapel(event.target.value)}
                error={false}
                required
              />
            </GridItem>
          </GridContainer>
        </CardBody>
        <CardFooter>
          <Button onClick={voltar}>Cancelar</Button>
          <Button color="success" type="submit">
            Salvar
          </Button>
        </CardFooter>
      </Card>
    </form>
  );
};

NovoUsuarioPapel.layout = Admin;
export default NovoUsuarioPapel;
