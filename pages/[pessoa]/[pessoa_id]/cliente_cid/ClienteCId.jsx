import { useRouter } from "next/router";

import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import CardHeader from "components/Card/CardHeader.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Table from "components/Table/Table.js";

import { makeStyles } from "@material-ui/core/styles";
import styles from "assets/jss/nextjs-material-dashboard/views/dashboardStyle.js";

const ClienteCid = (props) => {
  const router = useRouter();
  const useStyles = makeStyles(styles);
  const classes = useStyles();

  const novoClienteCid = () => {
    router.push(`/${props.pessoa}/${props.editar}/cliente_cid/novo`);
  };

  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={12}>
        <Card>
          <CardHeader color="info">
            <h4 className={classes.cardTitleWhite}>CIDs</h4>
          </CardHeader>
          <CardBody>
            <Table
              tableHeaderColor="info"
              tableHead={["ID", "Descrição", "Código", "Data"]}
              tableData={props.lista}
              linkUrl={`/${props.pessoa}/${props.editar}/cliente_cid`}
            />
          </CardBody>
          <CardFooter>
            <Button onClick={novoClienteCid}>Incluir CID</Button>
          </CardFooter>
        </Card>
      </GridItem>
    </GridContainer>
  );
};

const ClienteCidConverter = (lista) => {
  return lista.map((cliente_cid) => [
    cliente_cid.id,
    cliente_cid.cid.codigo,
    cliente_cid.cid.descricao,
    cliente_cid.data_diagnostico,
  ]);
};

export { ClienteCid, ClienteCidConverter };
