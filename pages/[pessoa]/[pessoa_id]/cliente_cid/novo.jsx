import { useState, useEffect } from "react";
import { useRouter } from "next/router";

import Swal from "sweetalert2";

import Admin from "layouts/Admin.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import CardHeader from "components/Card/CardHeader.js";
import CustomSelect from "../../../../components/CustomSelect/CustomSelect";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";

import { PostClienteCid } from "../../../../services/ClienteCId";

import { GetCids } from "../../../../services/Cid";

const NovoClienteCid = () => {
  // Ready
  useEffect(() => {
    async function fetchCids() {
      const retorno = await GetCids();

      if (retorno) {
        const result = retorno.results.map((cid) => ({
          value: cid.id,
          label: cid.codigo + " - " + cid.descricao,
        }));

        setCids(result);
      }
    }

    fetchCids();
  }, []);

  const router = useRouter();
  const { pessoa, pessoa_id } = router.query;

  const [cid, setCid] = useState([]);
  const [cids, setCids] = useState([]);

  function dataAtualFormatada() {
    var data = new Date(),
      dia = data.getDate().toString(),
      diaF = dia.length == 1 ? "0" + dia : dia,
      mes = (data.getMonth() + 1).toString(), //+1 pois no getMonth Janeiro começa com zero.
      mesF = mes.length == 1 ? "0" + mes : mes,
      anoF = data.getFullYear();
    return anoF + "-" + mesF + "-" + diaF;
  }

  const handleSubmit = (event) => {
    event.preventDefault();

    async function saveClienteCids() {
      const retorno = await PostClienteCid({
        cid,
        cliente: pessoa_id,
        data_diagnostico: dataAtualFormatada(),
      });
      if (retorno) {
        Swal.fire("Sucesso", "Criado com sucesso", "success").then((result) => {
          if (result.isConfirmed) {
            voltar();
          }
        });
      }
    }

    saveClienteCids();
  };

  const voltar = () => {
    router.push(`/${pessoa}/${pessoa_id}`);
  };

  const formControlProps = {
    fullWidth: true,
  };

  return (
    <form onSubmit={handleSubmit}>
      <Card>
        <CardHeader color="info">
          <h4>Novo ClienteCid!!</h4>
          <p>Novo ClienteCid</p>
        </CardHeader>
        <CardBody>
          <GridContainer>
            <GridItem xs={12} sm={12} md={2}>
              <CustomSelect
                labelText="Cid"
                id="cid"
                currencies={cids}
                formControlProps={formControlProps}
                onChange={(event) => setCid(event.target.value)}
                error={false}
                required
              />
            </GridItem>
          </GridContainer>
        </CardBody>
        <CardFooter>
          <Button onClick={voltar}>Cancelar</Button>
          <Button color="success" type="submit">
            Salvar
          </Button>
        </CardFooter>
      </Card>
    </form>
  );
};

NovoClienteCid.layout = Admin;
export default NovoClienteCid;
