import { useState, useEffect } from "react";
import { useRouter } from "next/router";

import Swal from "sweetalert2";

import Admin from "layouts/Admin.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import CardHeader from "components/Card/CardHeader.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import CustomSelect from "../../../../components/CustomSelect/CustomSelect";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";

import {
  PutClienteCid,
  GetClienteCidPorId,
  DeleteClienteCid,
} from "../../../../services/ClienteCId";
import { GetCids } from "../../../../services/Cid";

const NovoClienteCid = () => {
  const router = useRouter();
  const { pessoa, pessoa_id, editar } = router.query;

  const [id, setId] = useState([]);
  const [cid, setCid] = useState([]);
  const [cids, setCids] = useState([]);

  // Ready
  useEffect(() => {
    async function fetchGetClienteCid() {
      const retorno = await GetClienteCidPorId(editar);
      if (retorno) {
        setId(editar);
        setCid(retorno.cid);
      }
    }
    async function fetchCids() {
      const retorno = await GetCids();

      if (retorno) {
        const result = retorno.results.map((cid) => ({
          value: cid.id,
          label: cid.codigo,
        }));

        setCids(result);
      }
    }

    fetchGetClienteCid();
    fetchCids();
  }, []);

  const handleSubmit = (event) => {
    event.preventDefault();

    async function fetchGetUsuarioCid() {
      const retorno = await PutClienteCid(editar, {
        id: editar,
        cid,
        pessoa: pessoa_id,
      });
      if (retorno) {
        Swal.fire("Sucesso", "Atualizado com sucesso", "success").then(
          (result) => {
            if (result.isConfirmed) {
              voltar();
            }
          }
        );
      }
    }

    fetchGetUsuarioCid();
  };

  const formControlProps = {
    fullWidth: true,
  };

  const voltar = () => {
    router.push(`/${pessoa}/${pessoa_id}`);
  };

  const excluir = () => {
    Swal.fire({
      title: "Deseja excluir esse CID do cliente?",
      showDenyButton: true,
      confirmButtonText: `Sim`,
      denyButtonText: `Não`,
    }).then((result) => {
      if (result.isConfirmed) {
        DeleteClienteCid(editar);
        voltar();
      }
    });
  };

  return (
    <form onSubmit={handleSubmit}>
      <Card>
        <CardHeader color="info">
          <h4>Editar cid do usuario!!</h4>
          <p>Editar cid do usuario</p>
        </CardHeader>
        <CardBody>
          <GridContainer>
            <GridItem xs={12} sm={12} md={6}>
              <CustomInput
                labelText="Id"
                id="Id"
                type="text"
                value={id}
                formControlProps={formControlProps}
                error={false}
                required
                disabled
              />
            </GridItem>
            <GridItem xs={12} sm={12} md={6}>
              <CustomSelect
                labelText="Cid"
                id="cid"
                value={cid}
                currencies={cids}
                formControlProps={formControlProps}
                onChange={(event) => setCid(event.target.value)}
                error={false}
                required
              />
            </GridItem>
          </GridContainer>
        </CardBody>
        <CardFooter>
          <Button onClick={voltar}>Cancelar</Button>
          <Button color="danger" onClick={excluir}>
            Excluir
          </Button>
          <Button color="success" type="submit">
            Salvar
          </Button>
        </CardFooter>
      </Card>
    </form>
  );
};

NovoClienteCid.layout = Admin;
export default NovoClienteCid;
