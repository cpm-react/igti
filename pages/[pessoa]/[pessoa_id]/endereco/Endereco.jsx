import { useRouter } from "next/router";

import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import CardHeader from "components/Card/CardHeader.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Table from "components/Table/Table.js";

import { makeStyles } from "@material-ui/core/styles";
import styles from "assets/jss/nextjs-material-dashboard/views/dashboardStyle.js";

import { EstadoFiltroLabel } from "../../../enum";

const Endereco = (props) => {
  const router = useRouter();
  const useStyles = makeStyles(styles);
  const classes = useStyles();

  const novoEndereco = () => {
    router.push(`/${props.pessoa}/${props.editar}/endereco/novo`);
  };

  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={12}>
        <Card>
          <CardHeader color="info">
            <h4 className={classes.cardTitleWhite}>Endereços</h4>
          </CardHeader>
          <CardBody>
            <Table
              tableHeaderColor="info"
              tableHead={[
                "Id",
                "Título",
                "Logradouro",
                "Número",
                "Complemento",
                "Bairro",
                "Cep",
                "Cidade",
                "Estado",
                "Pais",
              ]}
              tableData={props.lista}
              linkUrl={`/${props.pessoa}/${props.editar}/endereco`}
            />
          </CardBody>
          <CardFooter>
            <Button onClick={novoEndereco}>Novo Endereço</Button>
          </CardFooter>
        </Card>
      </GridItem>
    </GridContainer>
  );
};

const EnderecoConverter = (lista) => {
  return lista.map((endereco) => [
    endereco.id,
    endereco.titulo,
    endereco.logradouro,
    endereco.numero,
    endereco.complemento,
    endereco.bairro,
    endereco.cep,
    endereco.cidade,
    EstadoFiltroLabel(endereco.estado),
    endereco.pais,
  ]);
};

export { Endereco, EnderecoConverter };
