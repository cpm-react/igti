import { useState, useEffect } from "react";
import { useRouter } from "next/router";

import Swal from "sweetalert2";

import Admin from "layouts/Admin.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import CardHeader from "components/Card/CardHeader.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import CustomSelect from "../../../../components/CustomSelect/CustomSelect";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";

import { PostEndereco } from "../../../../services/Endereco";
import { GetSessao } from "../../../../services/Usuario";
import { Estado } from "../../../enum";

const NovoEndereco = () => {
  // Ready
  useEffect(() => {
    GetSessao();
  }, []);

  const router = useRouter();
  const { pessoa, pessoa_id } = router.query;

  const [titulo, setTitulo] = useState([]);
  const [logradouro, setLogradouro] = useState([]);
  const [numero, setNumero] = useState([]);
  const [complemento, setComplemento] = useState([]);
  const [cep, setCep] = useState([]);
  const [bairro, setBairro] = useState([]);
  const [cidade, setCidade] = useState([]);
  const [estado, setEstado] = useState([]);

  const handleSubmit = (event) => {
    event.preventDefault();

    async function fetchGetEnderecos() {
      const retorno = await PostEndereco({
        titulo,
        logradouro,
        numero,
        complemento,
        cep,
        bairro,
        cidade,
        estado,
        pais: "Brasil",
        pessoa: pessoa_id,
      });
      if (retorno) {
        Swal.fire("Sucesso", "Criado com sucesso", "success").then((result) => {
          if (result.isConfirmed) {
            voltar();
          }
        });
      }
    }

    fetchGetEnderecos();
  };

  const voltar = () => {
    router.push(`/${pessoa}/${pessoa_id}`);
  };

  const formControlProps = {
    fullWidth: true,
  };

  return (
    <form onSubmit={handleSubmit}>
      <Card>
        <CardHeader color="info">
          <h4>Novo Endereço!!</h4>
          <p>Novo Endereço</p>
        </CardHeader>
        <CardBody>
          <GridContainer>
            <GridItem xs={12} sm={12} md={12}>
              <CustomInput
                labelText="Título"
                id="titulo"
                type="text"
                value={titulo}
                formControlProps={formControlProps}
                onChange={(event) => setTitulo(event.target.value)}
                error={false}
                maxlength="50"
                required
              />
            </GridItem>
          </GridContainer>
          <GridContainer>
            <GridItem xs={12} sm={12} md={6}>
              <CustomInput
                labelText="Logradouro"
                id="logradouro"
                type="text"
                value={logradouro}
                formControlProps={formControlProps}
                onChange={(event) => setLogradouro(event.target.value)}
                error={false}
                maxlength="200"
                required
              />
            </GridItem>
            <GridItem xs={12} sm={12} md={3}>
              <CustomInput
                labelText="Número"
                id="numero"
                type="number"
                value={numero}
                formControlProps={formControlProps}
                onChange={(event) => setNumero(event.target.value)}
                error={false}
                maxlength="100"
                required
              />
            </GridItem>
            <GridItem xs={12} sm={12} md={3}>
              <CustomInput
                labelText="Complemento"
                id="complemento"
                type="text"
                value={complemento}
                formControlProps={formControlProps}
                onChange={(event) => setComplemento(event.target.value)}
                error={false}
                maxlength="15"
              />
            </GridItem>
          </GridContainer>
          <GridContainer>
            <GridItem xs={12} sm={12} md={3}>
              <CustomInput
                labelText="CEP"
                id="cep"
                type="text"
                value={cep}
                formControlProps={formControlProps}
                onChange={(event) => setCep(event.target.value)}
                error={false}
                maxlength="9"
                required
              />
            </GridItem>
            <GridItem xs={12} sm={12} md={3}>
              <CustomInput
                labelText="Bairro"
                id="bairro"
                type="text"
                value={bairro}
                formControlProps={formControlProps}
                onChange={(event) => setBairro(event.target.value)}
                error={false}
                maxlength="100"
                required
              />
            </GridItem>
            <GridItem xs={12} sm={12} md={3}>
              <CustomInput
                labelText="Cidade"
                id="cidade"
                type="text"
                value={cidade}
                formControlProps={formControlProps}
                onChange={(event) => setCidade(event.target.value)}
                error={false}
                maxlength="100"
                required
              />
            </GridItem>
            <GridItem xs={12} sm={12} md={3}>
              <CustomSelect
                labelText="Estado"
                id="estado"
                value={estado}
                currencies={Estado()}
                formControlProps={formControlProps}
                onChange={(event) => setEstado(event.target.value)}
                error={false}
                required
              />
            </GridItem>
          </GridContainer>
        </CardBody>
        <CardFooter>
          <Button onClick={voltar}>Cancelar</Button>
          <Button color="success" type="submit">
            Salvar
          </Button>
        </CardFooter>
      </Card>
    </form>
  );
};

NovoEndereco.layout = Admin;
export default NovoEndereco;
