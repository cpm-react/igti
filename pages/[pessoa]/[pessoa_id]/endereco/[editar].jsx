import { useState, useEffect } from "react";
import { useRouter } from "next/router";

import Swal from "sweetalert2";

import Admin from "layouts/Admin.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import CardHeader from "components/Card/CardHeader.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import CustomSelect from "../../../../components/CustomSelect/CustomSelect";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";

import {
  PutEndereco,
  GetEnderecoPorId,
  DeleteEndereco,
} from "../../../../services/Endereco";
import { Estado } from "../../../enum";

const NovoEndereco = () => {
  const router = useRouter();
  const { pessoa, pessoa_id, editar } = router.query;

  const [id, setId] = useState([]);
  const [titulo, setTitulo] = useState([]);
  const [logradouro, setLogradouro] = useState([]);
  const [numero, setNumero] = useState([]);
  const [complemento, setComplemento] = useState([]);
  const [cep, setCep] = useState([]);
  const [bairro, setBairro] = useState([]);
  const [cidade, setCidade] = useState([]);
  const [estado, setEstado] = useState([]);

  // Ready
  useEffect(() => {
    async function fetchGetUsuarios() {
      const retorno = await GetEnderecoPorId(editar);
      if (retorno) {
        setId(editar);
        setTitulo(retorno.titulo);
        setLogradouro(retorno.logradouro);
        setNumero(retorno.numero);
        setComplemento(retorno.complemento);
        setCep(retorno.cep);
        setBairro(retorno.bairro);
        setCidade(retorno.cidade);
        setEstado(retorno.estado);
      }
    }

    fetchGetUsuarios();
  }, []);

  const handleSubmit = (event) => {
    event.preventDefault();

    async function fetchGetEnderecos() {
      const retorno = await PutEndereco(editar, {
        id: editar,
        titulo,
        logradouro,
        numero,
        complemento,
        cep,
        bairro,
        cidade,
        estado,
        pais: "Brasil",
        pessoa: pessoa_id,
      });
      if (retorno) {
        Swal.fire("Sucesso", "Atualizado com sucesso", "success").then(
          (result) => {
            if (result.isConfirmed) {
              voltar();
            }
          }
        );
      }
    }

    fetchGetEnderecos();
  };

  const formControlProps = {
    fullWidth: true,
  };

  const voltar = () => {
    router.push(`/${pessoa}/${pessoa_id}`);
  };

  const excluir = () => {
    Swal.fire({
      title: "Deseja excluir esse Endereco??",
      showDenyButton: true,
      confirmButtonText: `Sim`,
      denyButtonText: `Não`,
    }).then((result) => {
      if (result.isConfirmed) {
        DeleteEndereco(editar);
        voltar();
      }
    });
  };

  return (
    <form onSubmit={handleSubmit}>
      <Card>
        <CardHeader color="info">
          <h4>Editar Endereco!!</h4>
          <p>Editar Endereco</p>
        </CardHeader>
        <CardBody>
          <GridContainer>
            <GridItem xs={12} sm={12} md={6}>
              <CustomInput
                labelText="Id"
                id="Id"
                type="text"
                value={id}
                formControlProps={formControlProps}
                error={false}
                required
                disabled
              />
            </GridItem>
            <GridItem xs={12} sm={12} md={6}>
              <CustomInput
                labelText="Título"
                id="titulo"
                type="text"
                value={titulo}
                formControlProps={formControlProps}
                onChange={(event) => setTitulo(event.target.value)}
                error={false}
                maxlength="50"
                required
              />
            </GridItem>
          </GridContainer>
          <GridContainer>
            <GridItem xs={12} sm={12} md={6}>
              <CustomInput
                labelText="Logradouro"
                id="logradouro"
                type="text"
                value={logradouro}
                formControlProps={formControlProps}
                onChange={(event) => setLogradouro(event.target.value)}
                error={false}
                maxlength="200"
                required
              />
            </GridItem>
            <GridItem xs={12} sm={12} md={3}>
              <CustomInput
                labelText="Número"
                id="numero"
                type="number"
                value={numero}
                formControlProps={formControlProps}
                onChange={(event) => setNumero(event.target.value)}
                error={false}
                maxlength="100"
                required
              />
            </GridItem>
            <GridItem xs={12} sm={12} md={3}>
              <CustomInput
                labelText="Complemento"
                id="complemento"
                type="text"
                value={complemento}
                formControlProps={formControlProps}
                onChange={(event) => setComplemento(event.target.value)}
                error={false}
                maxlength="15"
              />
            </GridItem>
          </GridContainer>
          <GridContainer>
            <GridItem xs={12} sm={12} md={3}>
              <CustomInput
                labelText="CEP"
                id="cep"
                type="text"
                value={cep}
                formControlProps={formControlProps}
                onChange={(event) => setCep(event.target.value)}
                error={false}
                maxlength="9"
                required
              />
            </GridItem>
            <GridItem xs={12} sm={12} md={3}>
              <CustomInput
                labelText="Bairro"
                id="bairro"
                type="text"
                value={bairro}
                formControlProps={formControlProps}
                onChange={(event) => setBairro(event.target.value)}
                error={false}
                maxlength="100"
                required
              />
            </GridItem>
            <GridItem xs={12} sm={12} md={3}>
              <CustomInput
                labelText="Cidade"
                id="cidade"
                type="text"
                value={cidade}
                formControlProps={formControlProps}
                onChange={(event) => setCidade(event.target.value)}
                error={false}
                maxlength="100"
                required
              />
            </GridItem>
            <GridItem xs={12} sm={12} md={3}>
              <CustomSelect
                labelText="Estado"
                id="estado"
                value={estado}
                currencies={Estado()}
                formControlProps={formControlProps}
                onChange={(event) => setEstado(event.target.value)}
                error={false}
                required
              />
            </GridItem>
          </GridContainer>
        </CardBody>
        <CardFooter>
          <Button onClick={voltar}>Cancelar</Button>
          <Button color="danger" onClick={excluir}>
            Excluir
          </Button>
          <Button color="success" type="submit">
            Salvar
          </Button>
        </CardFooter>
      </Card>
    </form>
  );
};

NovoEndereco.layout = Admin;
export default NovoEndereco;
