import { useState, useEffect } from "react";
import { useRouter } from "next/router";

import Swal from "sweetalert2";

import Admin from "layouts/Admin.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import CardHeader from "components/Card/CardHeader.js";
import CustomSelect from "../../../../components/CustomSelect/CustomSelect";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";

import { PostUsuarioProfissional } from "../../../../services/UsuarioProfissional";

import { GetProfissionals } from "../../../../services/Profissional";

const NovoUsuarioProfissional = () => {
  async function fetchProfissionais() {
    const retorno = await GetProfissionals();

    if (retorno) {
      const result = retorno.results.map((profissional) => ({
        value: profissional.id,
        label: profissional.nome,
      }));

      setProfissionais(result);
    }
  }

  async function saveUsuarioProfissionals() {
    const retorno = await PostUsuarioProfissional({
      profissional,
      usuario: pessoa_id,
    });
    if (retorno) {
      Swal.fire("Sucesso", "Criado com sucesso", "success").then((result) => {
        if (result.isConfirmed) {
          voltar();
        }
      });
    }
  }

  // Ready
  useEffect(() => {
    fetchProfissionais();
  }, []);

  const router = useRouter();
  const { pessoa, pessoa_id } = router.query;

  const [profissional, setProfissional] = useState([]);
  const [profissionais, setProfissionais] = useState([]);

  const handleSubmit = (event) => {
    event.preventDefault();

    saveUsuarioProfissionals();
  };

  const voltar = () => {
    router.push(`/${pessoa}/${pessoa_id}`);
  };

  const formControlProps = {
    fullWidth: true,
  };

  return (
    <form onSubmit={handleSubmit}>
      <Card>
        <CardHeader color="info">
          <h4>Novo Profissional!!</h4>
          <p>Novo Profissional</p>
        </CardHeader>
        <CardBody>
          <GridContainer>
            <GridItem xs={12} sm={12} md={12}>
              <CustomSelect
                labelText="Profissional"
                id="profissional"
                currencies={profissionais}
                formControlProps={formControlProps}
                onChange={(event) => setProfissional(event.target.value)}
                error={false}
                required
              />
            </GridItem>
          </GridContainer>
        </CardBody>
        <CardFooter>
          <Button onClick={voltar}>Cancelar</Button>
          <Button color="success" type="submit">
            Salvar
          </Button>
        </CardFooter>
      </Card>
    </form>
  );
};

NovoUsuarioProfissional.layout = Admin;
export default NovoUsuarioProfissional;
