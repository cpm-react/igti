import { useState, useEffect } from "react";
import { useRouter } from "next/router";

import Swal from "sweetalert2";

import Admin from "layouts/Admin.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import CardHeader from "components/Card/CardHeader.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import CustomSelect from "../../../../components/CustomSelect/CustomSelect";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";

import {
  PutUsuarioProfissional,
  GetUsuarioProfissionalPorId,
  DeleteUsuarioProfissional,
} from "../../../../services/UsuarioProfissional";
import { GetProfissionals } from "../../../../services/Profissional";

const NovoUsuarioProfissional = () => {
  const router = useRouter();
  const { pessoa, pessoa_id, editar } = router.query;

  const [id, setId] = useState([]);
  const [profissional, setProfissional] = useState([]);
  const [profissionais, setProfissionais] = useState([]);

  async function fetchGetUsuarios() {
    const retorno = await GetUsuarioProfissionalPorId(editar);
    if (retorno) {
      setId(editar);
      setProfissional(retorno.profissional);
    }
  }
  async function fetchProfissionais() {
    const retorno = await GetProfissionals();

    if (retorno) {
      const result = retorno.results.map((profissional) => ({
        value: profissional.id,
        label: profissional.nome,
      }));

      setProfissionais(result);
    }
  }

  // Ready
  useEffect(() => {

    fetchGetUsuarios();
    fetchProfissionais();
  }, []);

  const handleSubmit = (event) => {
    event.preventDefault();
  };

  const formControlProps = {
    fullWidth: true,
  };

  const voltar = () => {
    router.push(`/${pessoa}/${pessoa_id}`);
  };

  const excluir = () => {
    Swal.fire({
      title: "Deseja excluir esse profissional do usuario?",
      showDenyButton: true,
      confirmButtonText: `Sim`,
      denyButtonText: `Não`,
    }).then((result) => {
      if (result.isConfirmed) {
        DeleteUsuarioProfissional(editar);
        voltar();
      }
    });
  };

  return (
    <form onSubmit={handleSubmit}>
      <Card>
        <CardHeader color="info">
          <h4>Editar profissional do usuario!!</h4>
          <p>Editar profissional do usuario</p>
        </CardHeader>
        <CardBody>
          <GridContainer>
            <GridItem xs={12} sm={12} md={6}>
              <CustomInput
                labelText="Id"
                id="Id"
                type="text"
                value={id}
                formControlProps={formControlProps}
                error={false}
                required
                disabled
              />
            </GridItem>
            <GridItem xs={12} sm={12} md={6}>
              <CustomSelect
                labelText="Profissional"
                id="profissional"
                value={profissional}
                currencies={profissionais}
                formControlProps={formControlProps}
                onChange={(event) => setProfissional(event.target.value)}
                error={false}
                required
              />
            </GridItem>
          </GridContainer>
        </CardBody>
        <CardFooter>
          <Button onClick={voltar}>Cancelar</Button>
          <Button color="danger" onClick={excluir}>
            Excluir
          </Button>
        </CardFooter>
      </Card>
    </form>
  );
};

NovoUsuarioProfissional.layout = Admin;
export default NovoUsuarioProfissional;
