import { useRouter } from "next/router";

import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import CardHeader from "components/Card/CardHeader.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Table from "components/Table/Table.js";

import { makeStyles } from "@material-ui/core/styles";
import styles from "assets/jss/nextjs-material-dashboard/views/dashboardStyle.js";

const UsuarioProfissional = (props) => {
  const useStyles = makeStyles(styles);
  const classes = useStyles();
  const router = useRouter();

  const novoUsuarioProfissional = () => {
    router.push(`/${props.pessoa}/${props.editar}/usuarioprofissional/novo`);
  };

  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={12}>
        <Card>
          <CardHeader color="info">
            <h4 className={classes.cardTitleWhite}>Profissionais</h4>
          </CardHeader>
          <CardBody>
            <Table
              tableHeaderColor="info"
              tableHead={["ID", "Nome", "Documento"]}
              tableData={props.lista}
              linkUrl={`/${props.pessoa}/${props.editar}/usuarioprofissional`}
            />
          </CardBody>
          <CardFooter>
            {props.lista.length == 0 && (
              <Button onClick={novoUsuarioProfissional}>
                Incluir Profissional
              </Button>
            )}
          </CardFooter>
        </Card>
      </GridItem>
    </GridContainer>
  );
};

const UsuarioProfissionalConverter = async (lista) => {
  return lista.map((usuario_profissional) => [
    usuario_profissional.id,
    usuario_profissional.profissional.nome,
    usuario_profissional.profissional.documento,
  ]);
};

export { UsuarioProfissional, UsuarioProfissionalConverter };
