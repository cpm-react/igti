import { useState, useEffect } from "react";
import { useRouter } from "next/router";

import Swal from "sweetalert2";

import Admin from "layouts/Admin.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import CardHeader from "components/Card/CardHeader.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import CustomSelect from "../../components/CustomSelect/CustomSelect";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";

import { makeStyles } from "@material-ui/core/styles";
import styles from "assets/jss/nextjs-material-dashboard/views/dashboardStyle.js";

import {
  GetUsuarioPorId,
  PutUsuario,
  DeleteUsuario,
} from "../../services/Usuario";
import { TipoPessoa } from "../enum";
import {
  UsuarioPapel,
  UsuarioPapelConverter,
} from "../[pessoa]/[pessoa_id]/usuariopapel/UsuarioPapel";
import {
  UsuarioProfissional,
  UsuarioProfissionalConverter,
} from "../[pessoa]/[pessoa_id]/usuarioprofissional/UsuarioProfissional";

const EditarUsuario = () => {
  const router = useRouter();
  const { editar } = router.query;
  const useStyles = makeStyles(styles);
  const classes = useStyles();

  const [id, setId] = useState([]);
  // Pessoa
  const [nascimento, setNascimento] = useState([]); // Data
  const [documento, setDocumento] = useState([]); // 15
  const [tipoPessoa, setTipoPessoa] = useState([]); // Enum
  const [nome, setNome] = useState([]); // 100
  // Usuario
  const [username, setUsername] = useState([]); // 25
  const [email, setEmail] = useState([]); // 100 - email
  // SubCadastros
  const [usuarioPapeis, setUsuarioPapeis] = useState([]);
  const [usuarioProfissionais, setUsuarioProfissionais] = useState([]);

  useEffect(() => {
    async function fetchGetUsuarios() {
      const retorno = await GetUsuarioPorId(editar);
      if (retorno) {
        setId(editar);
        setNascimento(retorno.nascimento);
        setDocumento(retorno.documento);
        setTipoPessoa(retorno.tipo_pessoa);
        setNome(retorno.nome);
        setUsername(retorno.username);
        setEmail(retorno.email);
        setUsuarioPapeis(await UsuarioPapelConverter(retorno.usuarios_papel));
        setUsuarioProfissionais(
          await UsuarioProfissionalConverter(retorno.usuarios_profissionais)
        );
      }
    }

    fetchGetUsuarios();
  }, []);

  const handleSubmit = (event) => {
    event.preventDefault();

    async function fetchGetUsuarios() {
      PutUsuario(editar, {
        id: editar,
        nascimento,
        documento,
        tipo_pessoa: tipoPessoa,
        nome,
        username,
        email,
        senha: "1",
      });

      Swal.fire("Sucesso", "Atualizado com sucesso", "success");
    }

    fetchGetUsuarios();
  };

  const voltar = () => {
    router.push("/usuario");
  };

  const excluir = () => {
    Swal.fire({
      title: "Deseja excluir esse Usuário??",
      showDenyButton: true,
      confirmButtonText: `Sim`,
      denyButtonText: `Não`,
    }).then((result) => {
      if (result.isConfirmed) {
        DeleteUsuario(editar);
        voltar();
      }
    });
  };

  const formControlProps = {
    fullWidth: true,
  };

  return (
    <>
      <form onSubmit={handleSubmit}>
        <GridContainer>
          <GridItem xs={12} sm={12} md={12}>
            <Card>
              <CardHeader color="info">
                <h4>Editar Usuario!!</h4>
                <p>Editar usuário do sistema</p>
              </CardHeader>
              <CardBody>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={12}>
                    <CustomInput
                      labelText="Id"
                      id="Id"
                      type="text"
                      value={id}
                      formControlProps={formControlProps}
                      error={false}
                      required
                      disabled
                    />
                  </GridItem>
                </GridContainer>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={5}>
                    <CustomInput
                      labelText="Nascimento"
                      id="nascimento"
                      type="date"
                      value={nascimento}
                      formControlProps={formControlProps}
                      onChange={(event) => setNascimento(event.target.value)}
                      error={false}
                      required
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={7}>
                    <CustomSelect
                      labelText="tipoPessoa"
                      id="tipoPessoa"
                      value={tipoPessoa}
                      currencies={TipoPessoa()}
                      formControlProps={formControlProps}
                      onChange={(event) => setTipoPessoa(event.target.value)}
                      error={false}
                      maxlength="1"
                      required
                    />
                  </GridItem>
                </GridContainer>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={5}>
                    <CustomInput
                      labelText="documento"
                      id="descdocumentoricao"
                      type="text"
                      value={documento}
                      formControlProps={formControlProps}
                      onChange={(event) => setDocumento(event.target.value)}
                      error={false}
                      maxlength="15"
                      required
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={7}>
                    <CustomInput
                      labelText="Nome"
                      id="nome"
                      type="text"
                      value={nome}
                      formControlProps={formControlProps}
                      onChange={(event) => setNome(event.target.value)}
                      error={false}
                      maxlength="100"
                      required
                    />
                  </GridItem>
                </GridContainer>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={6}>
                    <CustomInput
                      labelText="username"
                      id="username"
                      type="text"
                      value={username}
                      formControlProps={formControlProps}
                      onChange={(event) => setUsername(event.target.value)}
                      error={false}
                      maxlength="25"
                      required
                      disabled
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={6}>
                    <CustomInput
                      labelText="email"
                      id="email"
                      type="email"
                      value={email}
                      formControlProps={formControlProps}
                      onChange={(event) => setEmail(event.target.value)}
                      error={false}
                      maxlength="100"
                      required
                    />
                  </GridItem>
                </GridContainer>
              </CardBody>
              <CardFooter>
                <Button onClick={voltar}>Cancelar</Button>
                <Button color="danger" onClick={excluir}>
                  Excluir
                </Button>
                <Button color="success" type="submit">
                  Salvar
                </Button>
              </CardFooter>
            </Card>
          </GridItem>
        </GridContainer>
      </form>

      <UsuarioPapel
        editar={editar}
        pessoa={"usuario"}
        lista={usuarioPapeis}
      ></UsuarioPapel>
      <UsuarioProfissional
        editar={editar}
        pessoa={"usuario"}
        lista={usuarioProfissionais}
      ></UsuarioProfissional>
    </>
  );
};

EditarUsuario.layout = Admin;
export default EditarUsuario;
