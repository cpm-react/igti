import { useEffect } from "react";
import { useState } from "react";
import { useRouter } from "next/router";

import Swal from "sweetalert2";

import Admin from "layouts/Admin.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import CardHeader from "components/Card/CardHeader.js";
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import CustomSelect from "../../components/CustomSelect/CustomSelect";

import { PostUsuario } from "../../services/Usuario";
import { GetSessao } from "../../services/Usuario";
import { TipoPessoa } from "../enum";

const NovoUsuario = () => {
  // Ready
  useEffect(() => {
    GetSessao();
  }, []);

  const router = useRouter();

  // Pessoa
  const [nascimento, setNascimento] = useState([]); // Data
  const [documento, setDocumento] = useState([]); // 15
  const [tipoPessoa, setTipoPessoa] = useState([]); // Enum
  const [nome, setNome] = useState([]); // 100
  // Usuario
  const [username, setUsername] = useState([]); // 25
  const [senha, setSenha] = useState([]); // 20 - password
  const [email, setEmail] = useState([]); // 100 - email

  const handleSubmit = (event) => {
    event.preventDefault();

    async function fetchGetUsuarios() {
      const retorno = await PostUsuario({
        nascimento,
        documento,
        "tipo_pessoa": tipoPessoa,
        nome,
        username,
        senha,
        email,
      });
      Swal.fire("Sucesso", "Criado com sucesso", "success").then((result) => {
        if (result.isConfirmed) {
          router.push(`/usuario/${retorno.id}`);
        }
      });
    }

    fetchGetUsuarios();
  };

  const formControlProps = {
    fullWidth: true,
  };

  return (
    <form onSubmit={handleSubmit}>
      <Card>
        <CardHeader color="info">
          <h4>Novo Usuário!!</h4>
          <p>Novo Usuário do sistema</p>
        </CardHeader>
        <CardBody>
          <GridContainer>
            <GridItem xs={12} sm={12} md={5}>
              <CustomInput
                labelText="Nascimento"
                id="nascimento"
                type="date"
                value={nascimento}
                formControlProps={formControlProps}
                onChange={(event) => setNascimento(event.target.value)}
                error={false}
                required
              />
            </GridItem>
            <GridItem xs={12} sm={12} md={7}>
              <CustomSelect
                labelText="Tipo de Pessoa"
                id="tipoPessoa"
                value={tipoPessoa}
                currencies={TipoPessoa()}
                formControlProps={formControlProps}
                onChange={(event) => setTipoPessoa(event.target.value)}
                error={false}
                maxlength="1"
                required
              />
            </GridItem>
          </GridContainer>
          <GridContainer>
            <GridItem xs={12} sm={12} md={5}>
              <CustomInput
                labelText="documento"
                id="documento"
                type="text"
                value={documento}
                formControlProps={formControlProps}
                onChange={(event) => setDocumento(event.target.value)}
                error={false}
                maxlength="15"
                required
              />
            </GridItem>
            <GridItem xs={12} sm={12} md={7}>
              <CustomInput
                labelText="Nome"
                id="nome"
                type="text"
                value={nome}
                formControlProps={formControlProps}
                onChange={(event) => setNome(event.target.value)}
                error={false}
                maxlength="100"
                required
              />
            </GridItem>
          </GridContainer>
          <GridContainer>
            <GridItem xs={12} sm={12} md={3}>
              <CustomInput
                labelText="username"
                id="username"
                type="text"
                value={username}
                formControlProps={formControlProps}
                onChange={(event) => setUsername(event.target.value)}
                error={false}
                maxlength="25"
                required
              />
            </GridItem>
            <GridItem xs={12} sm={12} md={3}>
              <CustomInput
                labelText="senha"
                id="senha"
                type="password"
                value={senha}
                formControlProps={formControlProps}
                onChange={(event) => setSenha(event.target.value)}
                error={false}
                maxlength="20"
                required
              />
            </GridItem>
            <GridItem xs={12} sm={12} md={6}>
              <CustomInput
                labelText="email"
                id="email"
                type="email"
                value={email}
                formControlProps={formControlProps}
                onChange={(event) => setEmail(event.target.value)}
                error={false}
                maxlength="100"
                required
              />
            </GridItem>
          </GridContainer>
        </CardBody>
        <CardFooter>
          <Button
            onClick={() => {
              router.push("/usuario");
            }}
          >
            Cancelar
          </Button>
          <Button color="success" type="submit">
            Salvar
          </Button>
        </CardFooter>
      </Card>
    </form>
  );
};

NovoUsuario.layout = Admin;
export default NovoUsuario;
