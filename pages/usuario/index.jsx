import { useEffect, useState } from "react";
import { useRouter } from "next/router";

import Admin from "layouts/Admin.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import CardHeader from "components/Card/CardHeader.js";
import Table from "components/Table/Table.js";

import { GetUsuarios } from "../../services/Usuario";

const Usuario = () => {
  const [usuarios, setUsuarios] = useState([]);
  const router = useRouter();

  useEffect(() => {
    async function fetchGetUsuarios() {
      const retorno = await GetUsuarios();

      if (retorno) {
        const result = retorno.results.map((usuario) => [
          usuario.id,
          usuario.nome,
          usuario.documento,
          usuario.username,
        ]);

        setUsuarios(result);
      }
    }

    fetchGetUsuarios();
  }, []);

  const novoUsuario = () => {
    router.push("/usuario/novo");
  };

  return (
    <Card>
      <CardHeader color="info">
        <h4>Usuários!!</h4>
        <p>
          Usuários em sistemas de informação são agentes externos ao sistema que
          usufruem da tecnologia para realizar determinado trabalho.
        </p>
      </CardHeader>
      <CardBody>
        <Table
          tableHeaderColor="info"
          tableHead={["ID", "Nome", "Documento", "Usuário",]}
          tableData={usuarios}
          linkUrl="/usuario"
        />
      </CardBody>
      <CardFooter>
        <Button onClick={novoUsuario}>Novo</Button>
      </CardFooter>
    </Card>
  );
};

Usuario.layout = Admin;
export default Usuario;
