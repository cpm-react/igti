import React, { useState } from "react";

// layout for this page
import Admin from "layouts/Admin.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardBody from "components/Card/CardBody.js";
require("react-big-calendar/lib/css/react-big-calendar.css");
import { Calendar, momentLocalizer } from "react-big-calendar";
import moment from "moment";
import { GetToken } from "../../services/Sessao";

import Swal from "sweetalert2";

moment.locale("pt-BR");

const localizer = momentLocalizer(moment);

let lista = [
  {
    id: 0,
    title: "All Day Event very long title",
    allDay: true,
    start: new Date(2021, 4, 0),
    end: new Date(2021, 4, 1),
  },
  {
    id: 1,
    title: "Long Event",
    start: new Date(2021, 3, 7),
    end: new Date(2021, 3, 10),
  },
  {
    id: 2,
    title: "DTS STARTS",
    start: new Date(2021, 3, 13, 0, 0, 0),
    end: new Date(2021, 3, 20, 0, 0, 0),
  },
  {
    id: 3,
    title: "Evento dia 13 de maio de 2021 das 20h as 22h",
    allDay: false,
    start: new Date(2021, 3, 14, 20, 0, 0),
    end: new Date(2021, 3, 14, 21, 59, 59),
  },
  {
    id: 4,
    title: "Evento agora",
    start: new Date(moment().valueOf() + 5 * 60 * 1000),
    end: new Date(moment().valueOf() + 65 * 60 * 1000),
  },
];

const option = {
  year: "numeric",
  month: "long",
  weekday: "long",
  day: "numeric",
  hour: "numeric",
  minute: "numeric",
  second: "numeric",
  timeZoneName: "long",
};

// Função para manegar o vento
function cliqueEvento(evento) {
  Swal.fire(
    evento.title,
    evento.start.toLocaleDateString("pt-br", option),
    "success"
  );
}
class Agenda extends React.Component {
  constructor(...args) {
    super(...args);
    this.state = { lista, teste: "123" };
    console.log(this.state);
  }

  gerenciarCliequeNaTabela = ({ start, end }) => {
    Swal.fire({
      title: "Buscar paciente",
      input: "text",
      showCancelButton: true,
      inputValidator: (title) => {
        if (title) {
          this.setState({
            lista: [
              ...this.state.lista,
              {
                id: lista.length,
                title,
                start,
                end,
              },
            ],
          });
        }
      },
    });
  };

  componentDidMount() {
    this.setState({ teste: GetToken() });
  }

  render() {
    return (
      <Card>
        <CardHeader color="info">
          <h4>Agenda: {this.state.teste}!!</h4>
          <p>Horários disponíveis</p>
        </CardHeader>
        <CardBody>
          <div>
            <Calendar
              selectable={true}
              events={this.state.lista}
              step={30}
              showMultiDayTimes={true}
              localizer={localizer}
              style={{ height: 500 }}
              messages={{
                next: "Prox",
                previous: "Ant",
                today: "Hoje",
                month: "Mês",
                week: "Semana",
                day: "Dia",
              }}
              onSelectEvent={(event) => cliqueEvento(event)}
              onSelectSlot={this.gerenciarCliequeNaTabela}
              scrollToTime={new Date(moment().unix())}
              defaultView={"week"}
            />
          </div>
        </CardBody>
      </Card>
    );
  }
}

Agenda.layout = Admin;

export default Agenda;
