import { useState, useEffect } from "react";
import { useRouter } from "next/router";

import Swal from "sweetalert2";

import Admin from "layouts/Admin.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import CardHeader from "components/Card/CardHeader.js";
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import CustomInput from "components/CustomInput/CustomInput.js";

import { GetOrgaoPorId, PutOrgao, DeleteOrgao } from "../../services/Orgao";

const EditarOrgao = () => {
  const router = useRouter();
  const { editar } = router.query;

  const [id, setId] = useState([]);
  const [nome, setNome] = useState([]);
  const [codigo, setCodigo] = useState([]);

  useEffect(() => {
    async function fetchGetOrgaos() {
      const retorno = await GetOrgaoPorId(editar);
      if (retorno) {
        setId(editar);
        setNome(retorno.nome);
        setCodigo(retorno.codigo);
      }
    }

    fetchGetOrgaos();
  }, []);

  const handleSubmit = (event) => {
    event.preventDefault();

    async function fetchGetOrgaos() {
      PutOrgao(editar, { id: editar, nome, codigo });

      Swal.fire("Sucesso", "Atualizado com sucesso", "success");
    }

    fetchGetOrgaos();
  };

  const voltar = () => {
    router.push("/orgao");
  };

  const excluir = () => {
    Swal.fire({
      title: "Deseja excluir esse Órgão??",
      showDenyButton: true,
      confirmButtonText: `Sim`,
      denyButtonText: `Não`,
    }).then((result) => {
      if (result.isConfirmed) {
        DeleteOrgao(editar);
        voltar();
      }
    });
  };

  const formControlProps = {
    fullWidth: true,
  };

  return (
    <form onSubmit={handleSubmit}>
      <Card>
        <CardHeader color="info">
          <h4>Editar Órgão!!</h4>
          <p>Editar o cadastro de Conselhos Regionais</p>
        </CardHeader>
        <CardBody>
          <GridContainer>
            <GridItem xs={12} sm={12} md={3}>
              <CustomInput
                labelText="Id"
                id="Id"
                type="text"
                value={id}
                formControlProps={formControlProps}
                error={false}
                required
                disabled
              />
            </GridItem>
            <GridItem xs={12} sm={12} md={5}>
              <CustomInput
                labelText="Nome"
                id="nome"
                type="text"
                value={nome}
                formControlProps={formControlProps}
                onChange={(event) => setNome(event.target.value)}
                error={false}
                maxlength="100"
                required
              />
            </GridItem>
            <GridItem xs={12} sm={12} md={3}>
              <CustomInput
                labelText="Código"
                id="codigo"
                type="text"
                value={codigo}
                formControlProps={formControlProps}
                onChange={(event) => setCodigo(event.target.value)}
                error={false}
                maxlength="10"
                required
              />
            </GridItem>
          </GridContainer>
        </CardBody>
        <CardFooter>
          <Button onClick={voltar}>Cancelar</Button>
          <Button color="danger" onClick={excluir}>
            Excluir
          </Button>
          <Button color="success" type="submit">
            Salvar
          </Button>
        </CardFooter>
      </Card>
    </form>
  );
};

EditarOrgao.layout = Admin;
export default EditarOrgao;
