import { useEffect, useState } from "react";
import { useRouter } from "next/router";

import Admin from "layouts/Admin.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import CardHeader from "components/Card/CardHeader.js";
import Table from "components/Table/Table.js";

import { Edit } from "@material-ui/icons";

import { GetOrgaos } from "../../services/Orgao";

const Orgao = () => {
  const [orgaos, setOrgaos] = useState([]);
  const router = useRouter();

  useEffect(() => {
    async function fetchGetOrgaos() {
      const retorno = await GetOrgaos();

      if (retorno) {
        const result = retorno.results.map((orgao) => [
          orgao.id,
          orgao.nome,
          orgao.codigo,
        ]);
  
        setOrgaos(result);
      }

    }

    fetchGetOrgaos();
  }, []);

  const novoOrgao = () => {
    router.push("/orgao/novo");
  };

  return (
    <Card>
      <CardHeader color="info">
        <h4>Órgão!!</h4>
        <p>Conselhos Regionais</p>
      </CardHeader>
      <CardBody>
        <Table
          tableHeaderColor="info"
          tableHead={["ID", "Nome", "Código"]}
          tableData={orgaos}
          linkUrl="/orgao"
        />
      </CardBody>
      <CardFooter>
        <Button onClick={novoOrgao}>Novo</Button>
      </CardFooter>
    </Card>
  );
};

Orgao.layout = Admin;
export default Orgao;
