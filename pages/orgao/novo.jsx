import { useEffect } from "react";
import { useState } from "react";
import { useRouter } from "next/router";

import Swal from "sweetalert2";

import Admin from "layouts/Admin.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import CardHeader from "components/Card/CardHeader.js";
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import CustomInput from "components/CustomInput/CustomInput.js";

import { PostOrgao } from "../../services/Orgao";
import { GetSessao } from "../../services/Usuario";

const NovoOrgao = () => {
  // Ready
  useEffect(() => {
    GetSessao();
  }, []);

  const router = useRouter();

  const [nome, setNome] = useState([]);
  const [codigo, setCodigo] = useState([]);

  const handleSubmit = (event) => {
    event.preventDefault();

    async function fetchGetOrgaos() {
      const retorno = await PostOrgao({ nome, codigo });
      Swal.fire("Sucesso", "Criado com sucesso", "success").then((result) => {
        if (result.isConfirmed) {
          router.push(`/orgao/${retorno.id}`);
        }
      });
    }

    fetchGetOrgaos();
  };

  const formControlProps = {
    fullWidth: true,
  };

  return (
    <form onSubmit={handleSubmit}>
      <Card>
        <CardHeader color="info">
          <h4>Novo Órgão!!</h4>
          <p>Cadastros de Conselhos Regionais</p>
        </CardHeader>
        <CardBody>
          <GridContainer>
            <GridItem xs={12} sm={12} md={5}>
              <CustomInput
                labelText="Nome"
                id="nome"
                type="text"
                value={nome}
                formControlProps={formControlProps}
                onChange={(event) => setNome(event.target.value)}
                error={false}
                maxlength="100"
                required
              />
            </GridItem>
            <GridItem xs={12} sm={12} md={3}>
              <CustomInput
                labelText="Código"
                id="codigo"
                type="text"
                value={codigo}
                formControlProps={formControlProps}
                onChange={(event) => setCodigo(event.target.value)}
                error={false}
                maxlength="10"
                required
              />
            </GridItem>
          </GridContainer>
        </CardBody>
        <CardFooter>
          <Button
            onClick={() => {
              router.push("/orgao");
            }}
          >
            Cancelar
          </Button>
          <Button color="success" type="submit">
            Salvar
          </Button>
        </CardFooter>
      </Card>
    </form>
  );
};

NovoOrgao.layout = Admin;
export default NovoOrgao;
