import { useState, useEffect } from "react";
import { useRouter } from "next/router";

import Swal from "sweetalert2";

import Admin from "layouts/Admin.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import CardHeader from "components/Card/CardHeader.js";
import CustomSelect from "../../../../components/CustomSelect/CustomSelect";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";

import { PostFormularioPapel } from "../../../../services/FormularioPapel";
import { GetPapels } from "../../../../services/Papel";

const NovoFormularioPapel = () => {
  // Ready
  useEffect(() => {
    async function fetchPapeis() {
      const retorno = await GetPapels();

      if (retorno) {
        const result = retorno.results.map((papel) => ({
          value: papel.id,
          label: papel.codigo + " - " + papel.descricao,
        }));

        setPapeis(result);
      }
    }

    fetchPapeis();
  }, []);

  const router = useRouter();
  const { formulario_papel } = router.query;

  const [papel, setPapel] = useState([]);
  const [papeis, setPapeis] = useState([]);

  const handleSubmit = (event) => {
    event.preventDefault();

    async function fetchGetFormularioPapeis() {
      console.log({
        formulario: formulario_papel,
        papel,
      });
      const retorno = await PostFormularioPapel({
        formulario: formulario_papel,
        papel,
      });
      if (retorno) {
        Swal.fire("Sucesso", "Criado com sucesso", "success").then((result) => {
          if (result.isConfirmed) {
            voltar();
          }
        });
      }
    }

    fetchGetFormularioPapeis();
  };

  const voltar = () => {
    router.push(`/formulario/${formulario_papel}`);
  };

  const formControlProps = {
    fullWidth: true,
  };

  return (
    <form onSubmit={handleSubmit}>
      <Card>
        <CardHeader color="info">
          <h4>Nova Papel!!</h4>
          <p>Nova Papel</p>
        </CardHeader>
        <CardBody>
          <GridContainer>
            <GridItem xs={12} sm={12} md={12}>
              <CustomSelect
                labelText="Papel"
                id="papel"
                currencies={papeis}
                formControlProps={formControlProps}
                onChange={(event) => setPapel(event.target.value)}
                error={false}
                required
              />
            </GridItem>
          </GridContainer>
        </CardBody>
        <CardFooter>
          <Button onClick={voltar}>Cancelar</Button>
          <Button color="success" type="submit">
            Salvar
          </Button>
        </CardFooter>
      </Card>
    </form>
  );
};

NovoFormularioPapel.layout = Admin;
export default NovoFormularioPapel;
