import { useState, useEffect } from "react";
import { useRouter } from "next/router";

import Swal from "sweetalert2";

import Admin from "layouts/Admin.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import CardHeader from "components/Card/CardHeader.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import CustomSelect from "../../../../components/CustomSelect/CustomSelect";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";

import {
  PutFormularioPapel,
  GetFormularioPapelPorId,
  DeleteFormularioPapel,
} from "../../../../services/FormularioPapel";
import { GetPapels } from "../../../../services/Papel";

const NovoFormularioPapel = () => {
  const router = useRouter();
  const { formulario_papel, editar } = router.query;

  const [id, setId] = useState([]);
  const [papel, setPapel] = useState([]);
  const [papeis, setPapeis] = useState([]);

  // Ready
  useEffect(() => {
    async function fetchGetFormularioPapeis() {
      const retorno = await GetFormularioPapelPorId(editar);
      if (retorno) {
        setId(editar);
        setPapel(retorno.papel);
      }
    }
    async function fetchPapeis() {
      const retorno = await GetPapels();

      if (retorno) {
        const result = retorno.results.map((papel) => ({
          value: papel.id,
          label: papel.codigo + " - " + papel.descricao,
        }));

        setPapeis(result);
      }
    }

    fetchPapeis();
    fetchGetFormularioPapeis();
  }, []);

  const handleSubmit = (event) => {
    event.preventDefault();

    async function fetchGetFormularioPapeis() {
      const retorno = await PutFormularioPapel(editar, {
        id: editar,
        formulario: formulario_papel,
        papel,
      });
      if (retorno) {
        Swal.fire("Sucesso", "Atualizado com sucesso", "success").then(
          (result) => {
            if (result.isConfirmed) {
              voltar();
            }
          }
        );
      }
    }

    fetchGetFormularioPapeis();
  };

  const formControlProps = {
    fullWidth: true,
  };

  const voltar = () => {
    router.push(`/formulario/${formulario_papel}`);
  };

  const excluir = () => {
    Swal.fire({
      title: "Deseja excluir esse papel do formulario??",
      showDenyButton: true,
      confirmButtonText: `Sim`,
      denyButtonText: `Não`,
    }).then((result) => {
      if (result.isConfirmed) {
        DeleteFormularioPapel(editar);
        voltar();
      }
    });
  };

  return (
    <form onSubmit={handleSubmit}>
      <Card>
        <CardHeader color="info">
          <h4>Editar Papel!!</h4>
          <p>Editar Papel</p>
        </CardHeader>
        <CardBody>
          <GridContainer>
            <GridItem xs={12} sm={12} md={12}>
              <CustomInput
                labelText="Id"
                id="Id"
                type="text"
                value={id}
                formControlProps={formControlProps}
                error={false}
                required
                disabled
              />
            </GridItem>
          </GridContainer>
          <GridContainer>
            <GridItem xs={12} sm={12} md={12}>
              <CustomSelect
                labelText="Papel"
                id="papel"
                currencies={papeis}
                value={papel}
                formControlProps={formControlProps}
                onChange={(event) => setPapel(event.target.value)}
                error={false}
                required
              />
            </GridItem>
          </GridContainer>
        </CardBody>
        <CardFooter>
          <Button onClick={voltar}>Cancelar</Button>
          <Button color="danger" onClick={excluir}>
            Excluir
          </Button>
          <Button color="success" type="submit">
            Salvar
          </Button>
        </CardFooter>
      </Card>
    </form>
  );
};

NovoFormularioPapel.layout = Admin;
export default NovoFormularioPapel;
