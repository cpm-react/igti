import { useRouter } from "next/router";

import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import CardHeader from "components/Card/CardHeader.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Table from "components/Table/Table.js";

import { makeStyles } from "@material-ui/core/styles";
import styles from "assets/jss/nextjs-material-dashboard/views/dashboardStyle.js";

const FormularioPapel = (props) => {
  const router = useRouter();
  const useStyles = makeStyles(styles);
  const classes = useStyles();

  const novoFormularioPapel = () => {
    router.push(`/formulario/formulario_papeis/${props.editar}/novo`);
  };

  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={12}>
        <Card>
          <CardHeader color="info">
            <h4 className={classes.cardTitleWhite}>Papeis</h4>
          </CardHeader>
          <CardBody>
            <Table
              tableHeaderColor="info"
              tableHead={["ID", "Descrição", "Código"]}
              tableData={props.lista}
              linkUrl={`/formulario/formulario_papeis/${props.editar}`}
            />
          </CardBody>
          <CardFooter>
            <Button onClick={novoFormularioPapel}>Nova papel</Button>
          </CardFooter>
        </Card>
      </GridItem>
    </GridContainer>
  );
};

const FormularioPapelConverter = (lista) => {
  console.log(lista);
  return lista.map((papel_opcao) => [
    papel_opcao.id,
    papel_opcao.papel.codigo,
    papel_opcao.papel.descricao,
  ]);
};

export { FormularioPapel, FormularioPapelConverter };
