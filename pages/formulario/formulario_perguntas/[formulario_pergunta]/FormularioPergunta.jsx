import { useRouter } from "next/router";

import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import CardHeader from "components/Card/CardHeader.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Table from "components/Table/Table.js";

import { makeStyles } from "@material-ui/core/styles";
import styles from "assets/jss/nextjs-material-dashboard/views/dashboardStyle.js";

import { TipoPerguntaFiltroLabel } from "../../../enum";

const FormularioPergunta = (props) => {
  const router = useRouter();
  const useStyles = makeStyles(styles);
  const classes = useStyles();

  const novoFormularioPergunta = () => {
    router.push(`/formulario/formulario_perguntas/${props.editar}/novo`);
  };

  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={12}>
        <Card>
          <CardHeader color="info">
            <h4 className={classes.cardTitleWhite}>Perguntas</h4>
          </CardHeader>
          <CardBody>
            <Table
              tableHeaderColor="info"
              tableHead={["ID", "Tipo", "Pergunta"]}
              tableData={props.lista}
              linkUrl={`/formulario/formulario_perguntas/${props.editar}`}
            />
          </CardBody>
          <CardFooter>
            <Button onClick={novoFormularioPergunta}>Nova pergunta</Button>
          </CardFooter>
        </Card>
      </GridItem>
    </GridContainer>
  );
};

const FormularioPerguntaConverter = (lista) => {
  console.log(lista);
  return lista.map((pergunta_opcao) => [
    pergunta_opcao.id,
    TipoPerguntaFiltroLabel(pergunta_opcao.pergunta.tipo_pergunta),
    pergunta_opcao.pergunta.descricao,
  ]);
};

export { FormularioPergunta, FormularioPerguntaConverter };
