import { useState, useEffect } from "react";
import { useRouter } from "next/router";

import Swal from "sweetalert2";

import Admin from "layouts/Admin.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import CardHeader from "components/Card/CardHeader.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import CustomSelect from "../../../../components/CustomSelect/CustomSelect";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";

import {
  PutFormularioPergunta,
  GetFormularioPerguntaPorId,
  DeleteFormularioPergunta,
} from "../../../../services/FormularioPergunta";
import { GetPerguntas } from "../../../../services/Pergunta";
import { TipoPerguntaFiltroLabel } from "../../../enum";

const NovoFormularioPergunta = () => {
  const router = useRouter();
  const { formulario_pergunta, editar } = router.query;

  const [id, setId] = useState([]);
  const [pergunta, setPergunta] = useState([]);
  const [perguntas, setPerguntas] = useState([]);

  // Ready
  useEffect(() => {
    async function fetchGetFormularioPerguntas() {
      const retorno = await GetFormularioPerguntaPorId(editar);
      if (retorno) {
        setId(editar);
        setPergunta(retorno.pergunta);
      }
    }
    async function fetchPerguntas() {
      const retorno = await GetPerguntas();

      if (retorno) {
        const result = retorno.results.map((pergunta) => ({
          value: pergunta.id,
          label:
            TipoPerguntaFiltroLabel(pergunta.tipo_pergunta) +
            " - " +
            pergunta.descricao,
        }));

        setPerguntas(result);
      }
    }

    fetchPerguntas();
    fetchGetFormularioPerguntas();
  }, []);

  const handleSubmit = (event) => {
    event.preventDefault();

    async function fetchGetFormularioPerguntas() {
      const retorno = await PutFormularioPergunta(editar, {
        id: editar,
        formulario: formulario_pergunta,
        pergunta,
      });
      if (retorno) {
        Swal.fire("Sucesso", "Atualizado com sucesso", "success").then(
          (result) => {
            if (result.isConfirmed) {
              voltar();
            }
          }
        );
      }
    }

    fetchGetFormularioPerguntas();
  };

  const formControlProps = {
    fullWidth: true,
  };

  const voltar = () => {
    router.push(`/formulario/${formulario_pergunta}`);
  };

  const excluir = () => {
    Swal.fire({
      title: "Deseja excluir esse pergunta do formulario??",
      showDenyButton: true,
      confirmButtonText: `Sim`,
      denyButtonText: `Não`,
    }).then((result) => {
      if (result.isConfirmed) {
        DeleteFormularioPergunta(editar);
        voltar();
      }
    });
  };

  return (
    <form onSubmit={handleSubmit}>
      <Card>
        <CardHeader color="info">
          <h4>Editar Pergunta!!</h4>
          <p>Editar Pergunta</p>
        </CardHeader>
        <CardBody>
          <GridContainer>
            <GridItem xs={12} sm={12} md={12}>
              <CustomInput
                labelText="Id"
                id="Id"
                type="text"
                value={id}
                formControlProps={formControlProps}
                error={false}
                required
                disabled
              />
            </GridItem>
          </GridContainer>
          <GridContainer>
            <GridItem xs={12} sm={12} md={12}>
              <CustomSelect
                labelText="Pergunta"
                id="pergunta"
                currencies={perguntas}
                value={pergunta}
                formControlProps={formControlProps}
                onChange={(event) => setPergunta(event.target.value)}
                error={false}
                required
              />
            </GridItem>
          </GridContainer>
        </CardBody>
        <CardFooter>
          <Button onClick={voltar}>Cancelar</Button>
          <Button color="danger" onClick={excluir}>
            Excluir
          </Button>
          <Button color="success" type="submit">
            Salvar
          </Button>
        </CardFooter>
      </Card>
    </form>
  );
};

NovoFormularioPergunta.layout = Admin;
export default NovoFormularioPergunta;
