import { useState, useEffect } from "react";
import { useRouter } from "next/router";

import Swal from "sweetalert2";

import Admin from "layouts/Admin.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import CardHeader from "components/Card/CardHeader.js";
import CustomSelect from "../../../../components/CustomSelect/CustomSelect";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";

import { PostFormularioPergunta } from "../../../../services/FormularioPergunta";
import { GetPerguntas } from "../../../../services/Pergunta";
import { TipoPerguntaFiltroLabel } from "../../../enum";

const NovoFormularioPergunta = () => {
  // Ready
  useEffect(() => {
    async function fetchPerguntas() {
      const retorno = await GetPerguntas();

      if (retorno) {
        const result = retorno.results.map((pergunta) => ({
          value: pergunta.id,
          label:
            TipoPerguntaFiltroLabel(pergunta.tipo_pergunta) +
            " - " +
            pergunta.descricao,
        }));

        setPerguntas(result);
      }
    }

    fetchPerguntas();
  }, []);

  const router = useRouter();
  const { formulario_pergunta } = router.query;

  const [pergunta, setPergunta] = useState([]);
  const [perguntas, setPerguntas] = useState([]);

  const handleSubmit = (event) => {
    event.preventDefault();

    async function fetchGetFormularioPerguntas() {
      console.log({
        formulario: formulario_pergunta,
        pergunta,
      });
      const retorno = await PostFormularioPergunta({
        formulario: formulario_pergunta,
        pergunta,
      });
      if (retorno) {
        Swal.fire("Sucesso", "Criado com sucesso", "success").then((result) => {
          if (result.isConfirmed) {
            voltar();
          }
        });
      }
    }

    fetchGetFormularioPerguntas();
  };

  const voltar = () => {
    router.push(`/formulario/${formulario_pergunta}`);
  };

  const formControlProps = {
    fullWidth: true,
  };

  return (
    <form onSubmit={handleSubmit}>
      <Card>
        <CardHeader color="info">
          <h4>Nova Pergunta!!</h4>
          <p>Nova Pergunta</p>
        </CardHeader>
        <CardBody>
          <GridContainer>
            <GridItem xs={12} sm={12} md={12}>
              <CustomSelect
                labelText="Pergunta"
                id="pergunta"
                currencies={perguntas}
                formControlProps={formControlProps}
                onChange={(event) => setPergunta(event.target.value)}
                error={false}
                required
              />
            </GridItem>
          </GridContainer>
        </CardBody>
        <CardFooter>
          <Button onClick={voltar}>Cancelar</Button>
          <Button color="success" type="submit">
            Salvar
          </Button>
        </CardFooter>
      </Card>
    </form>
  );
};

NovoFormularioPergunta.layout = Admin;
export default NovoFormularioPergunta;
