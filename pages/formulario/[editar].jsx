import { useState, useEffect } from "react";
import { useRouter } from "next/router";

import Swal from "sweetalert2";

import Admin from "layouts/Admin.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import CardHeader from "components/Card/CardHeader.js";
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import CustomInput from "components/CustomInput/CustomInput.js";

import {
  GetFormularioPorId,
  PutFormulario,
  DeleteFormulario,
} from "../../services/Formulario";
import {
  FormularioPergunta,
  FormularioPerguntaConverter,
} from "./formulario_perguntas/[formulario_pergunta]/FormularioPergunta";
import {
  FormularioPapel,
  FormularioPapelConverter,
} from "./formulario_papeis/[formulario_papel]/FormularioPapel";

const EditarFormulario = () => {
  const router = useRouter();
  const { editar } = router.query;

  const [id, setId] = useState([]);
  const [descricao, setDescricao] = useState([]);
  const [codigo, setCodigo] = useState([]);

  // SubCadastros
  const [formularioPerguntas, setFormularioPerguntas] = useState([]);
  const [formularioPapeis, setFormularioPapeis] = useState([]);

  useEffect(() => {
    async function fetchGetFormularios() {
      const retorno = await GetFormularioPorId(editar);
      if (retorno) {
        setId(editar);
        setDescricao(retorno.descricao);
        setCodigo(retorno.codigo);

        setFormularioPerguntas(
          FormularioPerguntaConverter(retorno.perguntas_do_formulario)
        );
        setFormularioPapeis(
          FormularioPapelConverter(retorno.papeis_do_formulario)
        );
      }
    }

    fetchGetFormularios();
  }, []);

  const handleSubmit = (event) => {
    event.preventDefault();

    async function fetchGetFormularios() {
      PutFormulario(editar, { id: editar, descricao, codigo });

      Swal.fire("Sucesso", "Atualizado com sucesso", "success");
    }

    fetchGetFormularios();
  };

  const voltar = () => {
    router.push("/formulario");
  };

  const excluir = () => {
    Swal.fire({
      title: "Deseja excluir esse Formulario??",
      showDenyButton: true,
      confirmButtonText: `Sim`,
      denyButtonText: `Não`,
    }).then((result) => {
      if (result.isConfirmed) {
        DeleteFormulario(editar);
        voltar();
      }
    });
  };

  const formControlProps = {
    fullWidth: true,
  };

  return (
    <>
      <form onSubmit={handleSubmit}>
        <Card>
          <CardHeader color="info">
            <h4>Editar Formulario!!</h4>
          </CardHeader>
          <CardBody>
            <GridContainer>
              <GridItem xs={12} sm={12} md={3}>
                <CustomInput
                  labelText="Id"
                  id="Id"
                  type="text"
                  value={id}
                  formControlProps={formControlProps}
                  error={false}
                  required
                  disabled
                />
              </GridItem>
              <GridItem xs={12} sm={12} md={6}>
                <CustomInput
                  labelText="Descrição"
                  id="descricao"
                  type="text"
                  value={descricao}
                  formControlProps={formControlProps}
                  onChange={(event) => setDescricao(event.target.value)}
                  error={false}
                  maxlength="200"
                  required
                />
              </GridItem>
              <GridItem xs={12} sm={12} md={2}>
                <CustomInput
                  labelText="Código"
                  id="codigo"
                  type="text"
                  value={codigo}
                  formControlProps={formControlProps}
                  onChange={(event) => setCodigo(event.target.value)}
                  error={false}
                  maxlength="15"
                  required
                />
              </GridItem>
            </GridContainer>
          </CardBody>
          <CardFooter>
            <Button onClick={voltar}>Cancelar</Button>
            <Button color="danger" onClick={excluir}>
              Excluir
            </Button>
            <Button color="success" type="submit">
              Salvar
            </Button>
          </CardFooter>
        </Card>
      </form>

      <FormularioPergunta
        editar={editar}
        lista={formularioPerguntas}
      ></FormularioPergunta>
      <FormularioPapel
        editar={editar}
        lista={formularioPapeis}
      ></FormularioPapel>
    </>
  );
};

EditarFormulario.layout = Admin;
export default EditarFormulario;
