import { useEffect, useState } from "react";
import { useRouter } from "next/router";

import Admin from "layouts/Admin.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import CardHeader from "components/Card/CardHeader.js";
import Table from "components/Table/Table.js";

import { GetFormularios } from "../../services/Formulario";

const Formulario = () => {
  const [papeis, setFormularios] = useState([]);
  const router = useRouter();

  useEffect(() => {
    async function fetchGetFormularios() {
      const retorno = await GetFormularios();

      if (retorno) {
        const result = retorno.results.map((formulario) => [
          formulario.id,
          formulario.descricao,
          formulario.codigo,
        ]);

        setFormularios(result);
      }
    }

    fetchGetFormularios();
  }, []);

  const novoFormulario = () => {
    router.push("/formulario/novo");
  };

  return (
    <Card>
      <CardHeader color="info">
        <h4>Formulario!!</h4>
        <p>
          Formulário é um documento pré-impresso onde são preenchidos os dados e
          informações, que permite a formalização das comunicações, o registro e
          o controle das atividades das organizações, como empresas ou
          instituições estatais.
        </p>
      </CardHeader>
      <CardBody>
        <Table
          tableHeaderColor="info"
          tableHead={["ID", "Descrição", "Código"]}
          tableData={papeis}
          linkUrl="/formulario"
        />
      </CardBody>
      <CardFooter>
        <Button onClick={novoFormulario}>Novo</Button>
      </CardFooter>
    </Card>
  );
};

Formulario.layout = Admin;
export default Formulario;
