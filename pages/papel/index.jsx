import { useEffect, useState } from "react";
import { useRouter } from "next/router";

import Admin from "layouts/Admin.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import CardHeader from "components/Card/CardHeader.js";
import Table from "components/Table/Table.js";

import { GetPapels } from "../../services/Papel";

const Papel = () => {
  const [papeis, setPapels] = useState([]);
  const router = useRouter();

  useEffect(() => {
    async function fetchGetPapels() {
      const retorno = await GetPapels();

      if (retorno) {
          const result = retorno.results.map((papel) => [
            papel.id,
            papel.descricao,
            papel.codigo,
          ]);

          setPapels(result);
      }

    }

    fetchGetPapels();
  }, []);

  const novoPapel = () => {
    router.push("/papel/novo");
  };

  return (
    <Card>
      <CardHeader color="info">
        <h4>Papel!!</h4>
        <p>
          As regras de negócios são condições preestabelecidas que servem para
          determinar, autorizar ou limitar ações dentro de um processo.
        </p>
      </CardHeader>
      <CardBody>
        <Table
          tableHeaderColor="info"
          tableHead={["ID", "Descrição", "Código"]}
          tableData={papeis}
          linkUrl="/papel"
        />
      </CardBody>
      <CardFooter>
        <Button onClick={novoPapel}>Novo</Button>
      </CardFooter>
    </Card>
  );
};

Papel.layout = Admin;
export default Papel;
