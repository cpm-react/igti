import { useEffect } from "react";
import { useState } from "react";
import { useRouter } from "next/router";

import Swal from "sweetalert2";

import Admin from "layouts/Admin.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import CardHeader from "components/Card/CardHeader.js";
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import CustomInput from "components/CustomInput/CustomInput.js";

import { PostPapel } from "../../services/Papel";
import { GetSessao } from "../../services/Usuario";

const NovoPapel = () => {
  // Ready
  useEffect(() => {
    GetSessao();
  }, []);

  const router = useRouter();

  const [descricao, setDescricao] = useState([]);
  const [codigo, setCodigo] = useState([]);

  const handleSubmit = (event) => {
    event.preventDefault();

    async function fetchGetPapels() {
      const retorno = await PostPapel({ descricao, codigo });
      Swal.fire("Sucesso", "Criado com sucesso", "success").then((result) => {
        if (result.isConfirmed) {
          router.push(`/papel/${retorno.id}`);
        }
      });
    }

    fetchGetPapels();
  };

  const formControlProps = {
    fullWidth: true,
  };

  return (
    <form onSubmit={handleSubmit}>
      <Card>
        <CardHeader color="info">
          <h4>Novo Papel!!</h4>
          <p>
            Novo regra de negócio, são condições preestabelecidas que servem
            para determinar, autorizar ou limitar ações dentro de um processo.
          </p>
        </CardHeader>
        <CardBody>
          <GridContainer>
            <GridItem xs={12} sm={12} md={7}>
              <CustomInput
                labelText="Descrição"
                id="descricao"
                type="text"
                value={descricao}
                formControlProps={formControlProps}
                onChange={(event) => setDescricao(event.target.value)}
                error={false}
                maxlength="200"
                required
              />
            </GridItem>
            <GridItem xs={12} sm={12} md={2}>
              <CustomInput
                labelText="Código"
                id="codigo"
                type="text"
                value={codigo}
                formControlProps={formControlProps}
                onChange={(event) => setCodigo(event.target.value)}
                error={false}
                maxlength="15"
                required
              />
            </GridItem>
          </GridContainer>
        </CardBody>
        <CardFooter>
          <Button
            onClick={() => {
              router.push("/papel");
            }}
          >
            Cancelar
          </Button>
          <Button color="success" type="submit">
            Salvar
          </Button>
        </CardFooter>
      </Card>
    </form>
  );
};

NovoPapel.layout = Admin;
export default NovoPapel;
