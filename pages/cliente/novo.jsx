import { useEffect } from "react";
import { useState } from "react";
import { useRouter } from "next/router";

import Swal from "sweetalert2";

import Admin from "layouts/Admin.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import CardHeader from "components/Card/CardHeader.js";
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import CustomSelect from "../../components/CustomSelect/CustomSelect";

import { PostCliente } from "../../services/Cliente";
import { GetPlanos } from "../../services/Plano";
import { TipoPessoa } from "../enum";

const NovoCliente = () => {
  // Ready
  useEffect(() => {
    async function fetchPlanos() {
      const retorno = await GetPlanos();

      if (retorno) {
        const result = retorno.results.map((plano) => ({
          value: plano.id,
          label: plano.nome,
        }));
        console.log(result);

        setPlanos(result);
      }
    }

    fetchPlanos();
  }, []);

  const router = useRouter();

  // Pessoa
  const [nascimento, setNascimento] = useState([]); // Data
  const [documento, setDocumento] = useState([]); // 15
  const [tipoPessoa, setTipoPessoa] = useState([]); // Enum
  const [nome, setNome] = useState([]); // 100
  // Cliente
  const [carteirinha, setCarteirinha] = useState([]); // 25
  const [plano, setPlano] = useState([]); // FK
  const [planos, setPlanos] = useState([]); // FK

  const handleSubmit = (event) => {
    event.preventDefault();

    async function fetchGetClientes() {
      const retorno = await PostCliente({
        nascimento,
        documento,
        tipo_pessoa: tipoPessoa,
        nome,
        carteirinha,
        plano,
      });
      Swal.fire("Sucesso", "Criado com sucesso", "success").then((result) => {
        if (result.isConfirmed) {
          router.push(`/cliente/${retorno.id}`);
        }
      });
    }

    fetchGetClientes();
  };

  const formControlProps = {
    fullWidth: true,
  };

  return (
    <form onSubmit={handleSubmit}>
      <Card>
        <CardHeader color="info">
          <h4>Cliente!!</h4>
          <p>
            Novo cliente é aquele que procura o médico de livre e espontânea
            vontade. Não necessariamente porque esteja doente, mas, para
            prevenir doenças, melhorar sua saúde, qualidade de vida ou até mesmo
            sua aparência.
          </p>
        </CardHeader>
        <CardBody>
          <GridContainer>
            <GridItem xs={12} sm={12} md={5}>
              <CustomInput
                labelText="Nascimento"
                id="nascimento"
                type="date"
                value={nascimento}
                formControlProps={formControlProps}
                onChange={(event) => setNascimento(event.target.value)}
                error={false}
                required
              />
            </GridItem>
            <GridItem xs={12} sm={12} md={7}>
              <CustomSelect
                labelText="Tipo de Pessoa"
                id="tipoPessoa"
                value={tipoPessoa}
                currencies={TipoPessoa()}
                formControlProps={formControlProps}
                onChange={(event) => setTipoPessoa(event.target.value)}
                error={false}
                maxlength="1"
                required
              />
            </GridItem>
          </GridContainer>
          <GridContainer>
            <GridItem xs={12} sm={12} md={5}>
              <CustomInput
                labelText="documento"
                id="documento"
                type="text"
                value={documento}
                formControlProps={formControlProps}
                onChange={(event) => setDocumento(event.target.value)}
                error={false}
                maxlength="15"
                required
              />
            </GridItem>
            <GridItem xs={12} sm={12} md={7}>
              <CustomInput
                labelText="Nome"
                id="nome"
                type="text"
                value={nome}
                formControlProps={formControlProps}
                onChange={(event) => setNome(event.target.value)}
                error={false}
                maxlength="100"
                required
              />
            </GridItem>
          </GridContainer>
          <GridContainer>
            <GridItem xs={12} sm={12} md={6}>
              <CustomInput
                labelText="Carteirinha"
                id="carteirinha"
                type="text"
                value={carteirinha}
                formControlProps={formControlProps}
                onChange={(event) => setCarteirinha(event.target.value)}
                error={false}
                maxlength="25"
                required
              />
            </GridItem>
            <GridItem xs={12} sm={12} md={6}>
              <CustomSelect
                labelText="Plano"
                id="plano"
                value={plano}
                currencies={planos}
                formControlProps={formControlProps}
                onChange={(event) => setPlano(event.target.value)}
                error={false}
                maxlength="1"
                required
              />
            </GridItem>
          </GridContainer>
        </CardBody>
        <CardFooter>
          <Button
            onClick={() => {
              router.push("/cliente");
            }}
          >
            Cancelar
          </Button>
          <Button color="success" type="submit">
            Salvar
          </Button>
        </CardFooter>
      </Card>
    </form>
  );
};

NovoCliente.layout = Admin;
export default NovoCliente;
