import { useEffect, useState } from "react";
import { useRouter } from "next/router";

import Admin from "layouts/Admin.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import CardHeader from "components/Card/CardHeader.js";
import Table from "components/Table/Table.js";

import { GetClientes } from "../../services/Cliente";

const Cliente = () => {
  const [clientes, setClientes] = useState([]);
  const router = useRouter();

  useEffect(() => {
    async function fetchGetClientes() {
      const retorno = await GetClientes();

      if (retorno) {
        const result = retorno.results.map((cliente) => [
          cliente.id,
          cliente.nome,
          cliente.documento,
        ]);

        setClientes(result);
      }
    }

    fetchGetClientes();
  }, []);

  const novoCliente = () => {
    router.push("/cliente/novo");
  };

  return (
    <Card>
      <CardHeader color="info">
        <h4>Clientes!!</h4>
        <p>
          O cliente é aquele que procura o médico de livre e espontânea vontade.
          Não necessariamente porque esteja doente, mas, para prevenir doenças,
          melhorar sua saúde, qualidade de vida ou até mesmo sua aparência.
        </p>
      </CardHeader>
      <CardBody>
        <Table
          tableHeaderColor="info"
          tableHead={["ID", "Nome", "Documento"]}
          tableData={clientes}
          linkUrl="/cliente"
        />
      </CardBody>
      <CardFooter>
        <Button onClick={novoCliente}>Novo</Button>
      </CardFooter>
    </Card>
  );
};

Cliente.layout = Admin;
export default Cliente;
