import React, { useState } from "react";

import { makeStyles } from "@material-ui/core/styles";
import { Logar } from "../services/Usuario";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import CardHeader from "components/Card/CardHeader.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";

const importante = " !important";

const styles = {
  card: {
    height: `320px${importante}`,
    left: `50%${importante}`,
    marginLeft: `-250px${importante}`,
    marginTop: `-160px${importante}`,
    paddingBottom: "40px",
    position: `absolute${importante}`,
    textAlign: "center",
    top: `50%${importante}`,
    width: `500px${importante}`,
  },
};

const formControlProps = {
  fullWidth: true,
};

const Login = () => {
  const [usuario, setUsuario] = useState("");
  const [senha, setSenha] = useState("");

  const useStyles = makeStyles(styles);
  const classes = useStyles();

  const handleSubmit = (event) => {
    event.preventDefault();
    Logar({ usuario, senha });
  };

  return (
    <form onSubmit={handleSubmit}>
      <GridContainer>
        <Card className={classes.card}>
          <CardHeader color="info">
            <h4>Login</h4>
          </CardHeader>
          <CardBody>
            <GridItem>
              <CustomInput
                labelText="Usuário"
                id="usuario"
                type="text"
                value={usuario}
                formControlProps={formControlProps}
                onChange={(event) => setUsuario(event.target.value)}
                error={false}
                required
              />
            </GridItem>
            <GridItem>
              <CustomInput
                labelText="Senha"
                id="senha"
                type="password"
                value={senha}
                formControlProps={formControlProps}
                onChange={(event) => setSenha(event.target.value)}
                error={false}
                required
              />
            </GridItem>
          </CardBody>
          <CardFooter>
            <Button color="info" type="submit">
              Entrar
            </Button>
            <Button color="white">Esqueci minha senha</Button>
          </CardFooter>
        </Card>
      </GridContainer>
    </form>
  );
};

export default Login;
