import { useEffect, useState } from "react";
import { useRouter } from "next/router";

import Admin from "layouts/Admin.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import CardHeader from "components/Card/CardHeader.js";
import Table from "components/Table/Table.js";

import { GetPlanos } from "../../services/Plano";

const Plano = () => {
  const [planos, setPlanos] = useState([]);
  const router = useRouter();

  useEffect(() => {
    async function fetchGetPlanos() {
      const retorno = await GetPlanos();

      if (retorno) {
        const result = retorno.results.map((plano) => [
          plano.id,
          plano.nome,
          plano.codigo,
        ]);
  
        setPlanos(result);
      }
    }

    fetchGetPlanos();
  }, []);

  const novoPlano = () => {
    router.push("/plano/novo");
  };

  return (
    <Card>
      <CardHeader color="info">
        <h4>Plano!!</h4>
        <p>Cadastro de planos de saúde para o cliente</p>
      </CardHeader>
      <CardBody>
        <Table
          tableHeaderColor="info"
          tableHead={["ID", "Nome", "Código"]}
          tableData={planos}
          linkUrl="/plano"
        />
      </CardBody>
      <CardFooter>
        <Button onClick={novoPlano}>Novo</Button>
      </CardFooter>
    </Card>
  );
};

Plano.layout = Admin;
export default Plano;
