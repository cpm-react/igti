import { useEffect } from "react";
import { useState } from "react";
import { useRouter } from "next/router";

import Swal from "sweetalert2";

import Admin from "layouts/Admin.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import CardHeader from "components/Card/CardHeader.js";
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import CustomSelect from "../../components/CustomSelect/CustomSelect";

import { PostProfissional } from "../../services/Profissional";
import { GetOrgaos } from "../../services/Orgao";
import { TipoPessoa, Estado } from "../enum";

const NovoProfissional = () => {
  // Ready
  useEffect(() => {
    async function fetchOrgaos() {
      const retorno = await GetOrgaos();

      if (retorno) {
        const result = retorno.results.map((orgao) => ({
          value: orgao.id,
          label: orgao.codigo,
        }));

        setOrgaos(result);
      }
    }

    fetchOrgaos();
  }, []);

  const router = useRouter();

  // Pessoa
  const [nascimento, setNascimento] = useState([]); // Data
  const [documento, setDocumento] = useState([]); // 15
  const [tipoPessoa, setTipoPessoa] = useState([]); // Enum
  const [nome, setNome] = useState([]); // 100
  // Profissional
  const [numero, setNumero] = useState([]); // 25
  const [estado, setEstado] = useState([]); // Enum
  const [orgao, setOrgao] = useState([]); // FK
  const [orgaos, setOrgaos] = useState([]); // FK

  const handleSubmit = (event) => {
    event.preventDefault();

    async function fetchGetProfissionals() {
      const retorno = await PostProfissional({
        nascimento,
        documento,
        nome,
        tipo_pessoa: tipoPessoa,
        numero_orgao: numero,
        estado_orgao: estado,
        orgao,
      });
      Swal.fire("Sucesso", "Criado com sucesso", "success").then((result) => {
        if (result.isConfirmed) {
          router.push(`/profissional/${retorno.id}`);
        }
      });
    }

    fetchGetProfissionals();
  };

  const formControlProps = {
    fullWidth: true,
  };

  return (
    <form onSubmit={handleSubmit}>
      <Card>
        <CardHeader color="info">
          <h4>Profissional!!</h4>
          <p>Novo profissional.</p>
        </CardHeader>
        <CardBody>
          <GridContainer>
            <GridItem xs={12} sm={12} md={5}>
              <CustomInput
                labelText="Nascimento"
                id="nascimento"
                type="date"
                value={nascimento}
                formControlProps={formControlProps}
                onChange={(event) => setNascimento(event.target.value)}
                error={false}
                required
              />
            </GridItem>
            <GridItem xs={12} sm={12} md={7}>
              <CustomSelect
                labelText="Tipo de Pessoa"
                id="tipoPessoa"
                value={tipoPessoa}
                currencies={TipoPessoa()}
                formControlProps={formControlProps}
                onChange={(event) => setTipoPessoa(event.target.value)}
                error={false}
                maxlength="1"
                required
              />
            </GridItem>
          </GridContainer>
          <GridContainer>
            <GridItem xs={12} sm={12} md={5}>
              <CustomInput
                labelText="documento"
                id="documento"
                type="text"
                value={documento}
                formControlProps={formControlProps}
                onChange={(event) => setDocumento(event.target.value)}
                error={false}
                maxlength="15"
                required
              />
            </GridItem>
            <GridItem xs={12} sm={12} md={7}>
              <CustomInput
                labelText="Nome"
                id="nome"
                type="text"
                value={nome}
                formControlProps={formControlProps}
                onChange={(event) => setNome(event.target.value)}
                error={false}
                maxlength="100"
                required
              />
            </GridItem>
          </GridContainer>
          <GridContainer>
            <GridItem xs={12} sm={12} md={4}>
              <CustomInput
                labelText="Número"
                id="numero"
                type="text"
                value={numero}
                formControlProps={formControlProps}
                onChange={(event) => setNumero(event.target.value)}
                error={false}
                maxlength="15"
                required
              />
            </GridItem>
            <GridItem xs={12} sm={12} md={4}>
              <CustomSelect
                labelText="Órgão"
                id="orgao"
                value={orgao}
                currencies={orgaos}
                formControlProps={formControlProps}
                onChange={(event) => setOrgao(event.target.value)}
                required
              />
            </GridItem>
            <GridItem xs={12} sm={12} md={4}>
              <CustomSelect
                labelText="Estado"
                id="estado"
                value={estado}
                currencies={Estado()}
                formControlProps={formControlProps}
                onChange={(event) => setEstado(event.target.value)}
                required
              />
            </GridItem>
          </GridContainer>
        </CardBody>
        <CardFooter>
          <Button
            onClick={() => {
              router.push("/profissional");
            }}
          >
            Cancelar
          </Button>
          <Button color="success" type="submit">
            Salvar
          </Button>
        </CardFooter>
      </Card>
    </form>
  );
};

NovoProfissional.layout = Admin;
export default NovoProfissional;
