import { useState, useEffect } from "react";
import { useRouter } from "next/router";

import Swal from "sweetalert2";

import Admin from "layouts/Admin.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import CardHeader from "components/Card/CardHeader.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import CustomSelect from "../../components/CustomSelect/CustomSelect";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";

import { makeStyles } from "@material-ui/core/styles";
import styles from "assets/jss/nextjs-material-dashboard/views/dashboardStyle.js";

import {
  Contato,
  ContatoConverter,
} from "../[pessoa]/[pessoa_id]/contato/Contato";
import {
  Endereco,
  EnderecoConverter,
} from "../[pessoa]/[pessoa_id]/endereco/Endereco";

import {
  GetProfissionalPorId,
  PutProfissional,
  DeleteProfissional,
} from "../../services/Profissional";

import { GetOrgaos } from "../../services/Orgao";
import { TipoPessoa, Estado } from "../enum";

const EditarProfissional = () => {
  const router = useRouter();
  const { editar } = router.query;
  const useStyles = makeStyles(styles);
  const classes = useStyles();

  const [id, setId] = useState([]);
  // Pessoa
  const [nascimento, setNascimento] = useState([]); // Data
  const [documento, setDocumento] = useState([]); // 15
  const [tipoPessoa, setTipoPessoa] = useState([]); // Enum
  const [nome, setNome] = useState([]); // 100
  // Profissional
  const [numero, setNumero] = useState([]); // 25
  const [estado, setEstado] = useState([]); // Enum
  const [orgao, setOrgao] = useState([]); // FK
  const [orgaos, setOrgaos] = useState([]); // FK
  // SubCadastros
  const [enderecos, setEndrecos] = useState([]);
  const [contatos, setContatos] = useState([]);

  useEffect(() => {
    async function fetchGetProfissionals() {
      const retorno = await GetProfissionalPorId(editar);
      if (retorno) {
        setId(editar);
        setNascimento(retorno.nascimento);
        setDocumento(retorno.documento);
        setTipoPessoa(retorno.tipo_pessoa);
        setNome(retorno.nome);
        setNumero(retorno.numero_orgao);
        setEstado(retorno.estado_orgao);
        setOrgao(retorno.orgao);
        setEndrecos(EnderecoConverter(retorno.enderecos));
        setContatos(ContatoConverter(retorno.contatos));
      }
    }
    async function fetchOrgaos() {
      const retorno = await GetOrgaos();

      if (retorno) {
        const result = retorno.results.map((orgao) => ({
          value: orgao.id,
          label: orgao.codigo,
        }));

        setOrgaos(result);
      }
    }

    fetchGetProfissionals();
    fetchOrgaos();
  }, []);

  const handleSubmit = (event) => {
    event.preventDefault();

    async function fetchGetProfissionals() {
      PutProfissional(editar, {
        id: editar,
        nascimento,
        documento,
        nome,
        tipo_pessoa: tipoPessoa,
        numero_orgao: numero,
        estado_orgao: estado,
        orgao,
      });

      Swal.fire("Sucesso", "Atualizado com sucesso", "success");
    }

    fetchGetProfissionals();
  };

  const voltar = () => {
    router.push("/profissional");
  };

  const excluir = () => {
    Swal.fire({
      title: "Deseja excluir esse Usuário??",
      showDenyButton: true,
      confirmButtonText: `Sim`,
      denyButtonText: `Não`,
    }).then((result) => {
      if (result.isConfirmed) {
        DeleteProfissional(editar);
        voltar();
      }
    });
  };

  const novoEndereco = () => {
    router.push(`/profissional/${editar}/endereco/novo`);
  };

  const formControlProps = {
    fullWidth: true,
  };

  return (
    <>
      <form onSubmit={handleSubmit}>
        <GridContainer>
          <GridItem xs={12} sm={12} md={12}>
            <Card>
              <CardHeader color="info">
                <h4>Editar Profissional!!</h4>
                <p>Editar profissional.</p>
              </CardHeader>
              <CardBody>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={12}>
                    <CustomInput
                      labelText="Id"
                      id="Id"
                      type="text"
                      value={id}
                      formControlProps={formControlProps}
                      error={false}
                      required
                      disabled
                    />
                  </GridItem>
                </GridContainer>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={5}>
                    <CustomInput
                      labelText="Nascimento"
                      id="nascimento"
                      type="date"
                      value={nascimento}
                      formControlProps={formControlProps}
                      onChange={(event) => setNascimento(event.target.value)}
                      error={false}
                      required
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={7}>
                    <CustomSelect
                      labelText="tipoPessoa"
                      id="tipoPessoa"
                      value={tipoPessoa}
                      currencies={TipoPessoa()}
                      formControlProps={formControlProps}
                      onChange={(event) => setTipoPessoa(event.target.value)}
                      error={false}
                      maxlength="1"
                      required
                    />
                  </GridItem>
                </GridContainer>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={5}>
                    <CustomInput
                      labelText="documento"
                      id="descdocumentoricao"
                      type="text"
                      value={documento}
                      formControlProps={formControlProps}
                      onChange={(event) => setDocumento(event.target.value)}
                      error={false}
                      maxlength="15"
                      required
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={7}>
                    <CustomInput
                      labelText="Nome"
                      id="nome"
                      type="text"
                      value={nome}
                      formControlProps={formControlProps}
                      onChange={(event) => setNome(event.target.value)}
                      error={false}
                      maxlength="100"
                      required
                    />
                  </GridItem>
                </GridContainer>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={4}>
                    <CustomInput
                      labelText="Número"
                      id="numero"
                      type="text"
                      value={numero}
                      formControlProps={formControlProps}
                      onChange={(event) => setNumero(event.target.value)}
                      error={false}
                      maxlength="15"
                      required
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={4}>
                    <CustomSelect
                      labelText="Órgão"
                      id="orgao"
                      value={orgao}
                      currencies={orgaos}
                      formControlProps={formControlProps}
                      onChange={(event) => setOrgao(event.target.value)}
                      required
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={4}>
                    <CustomSelect
                      labelText="Estado"
                      id="estado"
                      value={estado}
                      currencies={Estado()}
                      formControlProps={formControlProps}
                      onChange={(event) => setEstado(event.target.value)}
                      required
                    />
                  </GridItem>
                </GridContainer>
              </CardBody>
              <CardFooter>
                <Button onClick={voltar}>Cancelar</Button>
                <Button color="danger" onClick={excluir}>
                  Excluir
                </Button>
                <Button color="success" type="submit">
                  Salvar
                </Button>
              </CardFooter>
            </Card>
          </GridItem>
        </GridContainer>
      </form>

      <Contato
        editar={editar}
        pessoa={"profissional"}
        lista={contatos}
      ></Contato>
      <Endereco
        editar={editar}
        pessoa={"profissional"}
        lista={enderecos}
      ></Endereco>
    </>
  );
};

EditarProfissional.layout = Admin;
export default EditarProfissional;
