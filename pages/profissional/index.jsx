import { useEffect, useState } from "react";
import { useRouter } from "next/router";

import Admin from "layouts/Admin.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import CardHeader from "components/Card/CardHeader.js";
import Table from "components/Table/Table.js";

import { GetProfissionals } from "../../services/Profissional";

const Profissional = () => {
  const [profissionals, setProfissionals] = useState([]);
  const router = useRouter();

  useEffect(() => {
    async function fetchGetProfissionals() {
      const retorno = await GetProfissionals();

      if (retorno) {
        const result = retorno.results.map((profissional) => [
          profissional.id,
          profissional.nome,
          profissional.documento,
        ]);

        setProfissionals(result);
      }
    }

    fetchGetProfissionals();
  }, []);

  const novoProfissional = () => {
    router.push("/profissional/novo");
  };

  return (
    <Card>
      <CardHeader color="info">
        <h4>Profissionais!!</h4>
        <p>
          Um profissional é o membro de uma profissão ou qualquer pessoa que
          ganha a vida com uma determinada atividade profissional.
        </p>
      </CardHeader>
      <CardBody>
        <Table
          tableHeaderColor="info"
          tableHead={["ID", "Nome", "Documento"]}
          tableData={profissionals}
          linkUrl="/profissional"
        />
      </CardBody>
      <CardFooter>
        <Button onClick={novoProfissional}>Novo</Button>
      </CardFooter>
    </Card>
  );
};

Profissional.layout = Admin;
export default Profissional;
