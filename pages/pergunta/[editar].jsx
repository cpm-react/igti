import { useState, useEffect } from "react";
import { useRouter } from "next/router";

import Swal from "sweetalert2";

import Admin from "layouts/Admin.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import CardHeader from "components/Card/CardHeader.js";
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import CustomSelect from "../../components/CustomSelect/CustomSelect";

import {
  GetPerguntaPorId,
  PutPergunta,
  DeletePergunta,
} from "../../services/Pergunta";
import { TipoPergunta } from "../enum";
import {
  PerguntaOpcao,
  PerguntaOpcaoConverter,
} from "./opcoes/[pergunta]/PerguntaOpcao";

const EditarPergunta = () => {
  const router = useRouter();
  const { editar } = router.query;

  const [id, setId] = useState([]);
  const [descricao, setDescricao] = useState([]);
  const [tipoPergunta, setTipoPergunta] = useState([]);

  // SubCadastros
  const [perguntaopcoes, setPerguntaOpcoes] = useState([]);

  useEffect(() => {
    async function fetchGetPerguntas() {
      const retorno = await GetPerguntaPorId(editar);
      if (retorno) {
        setId(editar);
        setDescricao(retorno.descricao);
        setTipoPergunta(retorno.tipo_pergunta);

        setPerguntaOpcoes(PerguntaOpcaoConverter(retorno.opcoes));
      }
    }

    fetchGetPerguntas();
  }, []);

  const handleSubmit = (event) => {
    event.preventDefault();

    async function fetchGetPerguntas() {
      PutPergunta(editar, {
        id: editar,
        descricao,
        tipo_pergunta: tipoPergunta,
      });

      Swal.fire("Sucesso", "Atualizado com sucesso", "success");
    }

    fetchGetPerguntas();
  };

  const voltar = () => {
    router.push("/pergunta");
  };

  const excluir = () => {
    Swal.fire({
      title: "Deseja excluir essa Pergunta??",
      showDenyButton: true,
      confirmButtonText: `Sim`,
      denyButtonText: `Não`,
    }).then((result) => {
      if (result.isConfirmed) {
        DeletePergunta(editar);
        voltar();
      }
    });
  };

  const formControlProps = {
    fullWidth: true,
  };

  return (
    <>
      <form onSubmit={handleSubmit}>
        <Card>
          <CardHeader color="info">
            <h4>Editar Pergunta!!</h4>
            <p>Perguntas para os formulários</p>
          </CardHeader>
          <CardBody>
            <GridContainer>
              <GridItem xs={12} sm={12} md={2}>
                <CustomInput
                  labelText="Id"
                  id="Id"
                  type="text"
                  value={id}
                  formControlProps={formControlProps}
                  error={false}
                  required
                  disabled
                />
              </GridItem>
              <GridItem xs={12} sm={12} md={6}>
                <CustomInput
                  labelText="Descrição"
                  id="descricao"
                  type="text"
                  value={descricao}
                  formControlProps={formControlProps}
                  onChange={(event) => setDescricao(event.target.value)}
                  error={false}
                  maxlength="90"
                  required
                />
              </GridItem>
              <GridItem xs={12} sm={12} md={4}>
                <CustomSelect
                  labelText="Tipo de pergunta"
                  id="tipo_pergunta"
                  value={tipoPergunta}
                  currencies={TipoPergunta()}
                  formControlProps={formControlProps}
                  onChange={(event) => setTipoPergunta(event.target.value)}
                  error={false}
                  maxlength="1"
                  required
                />
              </GridItem>
            </GridContainer>
          </CardBody>
          <CardFooter>
            <Button onClick={voltar}>Cancelar</Button>
            <Button color="danger" onClick={excluir}>
              Excluir
            </Button>
            <Button color="success" type="submit">
              Salvar
            </Button>
          </CardFooter>
        </Card>
      </form>

      {(tipoPergunta == "RADIO" ||
        tipoPergunta == "COMBO" ||
        tipoPergunta == "CHECKBOX") && (
        <PerguntaOpcao editar={editar} lista={perguntaopcoes}></PerguntaOpcao>
      )}
    </>
  );
};

EditarPergunta.layout = Admin;
export default EditarPergunta;
