import { useRouter } from "next/router";

import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import CardHeader from "components/Card/CardHeader.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Table from "components/Table/Table.js";

import { makeStyles } from "@material-ui/core/styles";
import styles from "assets/jss/nextjs-material-dashboard/views/dashboardStyle.js";

const PerguntaOpcao = (props) => {
  const router = useRouter();
  const useStyles = makeStyles(styles);
  const classes = useStyles();

  const novoPerguntaOpcao = () => {
    router.push(`/pergunta/opcoes/${props.editar}/novo`);
  };

  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={12}>
        <Card>
          <CardHeader color="info">
            <h4 className={classes.cardTitleWhite}>Opções</h4>
          </CardHeader>
          <CardBody>
            <Table
              tableHeaderColor="info"
              tableHead={["Id", "Valor"]}
              tableData={props.lista}
              linkUrl={`/pergunta/opcoes/${props.editar}`}
            />
          </CardBody>
          <CardFooter>
            <Button onClick={novoPerguntaOpcao}>Nova Opção</Button>
          </CardFooter>
        </Card>
      </GridItem>
    </GridContainer>
  );
};

const PerguntaOpcaoConverter = (lista) => {
  return lista.map((pergunta_opcao) => [
    pergunta_opcao.id,
    pergunta_opcao.opcao,
  ]);
};

export { PerguntaOpcao, PerguntaOpcaoConverter };
