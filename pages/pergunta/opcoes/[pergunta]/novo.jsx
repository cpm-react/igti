import { useState, useEffect } from "react";
import { useRouter } from "next/router";

import Swal from "sweetalert2";

import Admin from "layouts/Admin.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import CardHeader from "components/Card/CardHeader.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";

import { PostPerguntaOpcao } from "../../../../services/PerguntaOpcao";
import { GetSessao } from "../../../../services/Usuario";

const NovoPerguntaOpcao = () => {
  // Ready
  useEffect(() => {
    GetSessao();
  }, []);

  const router = useRouter();
  const { pergunta } = router.query;

  const [opcao, setOpcao] = useState([]);

  const handleSubmit = (event) => {
    event.preventDefault();

    async function fetchGetPerguntaOpcaos() {
      const retorno = await PostPerguntaOpcao({
        opcao,
        pergunta,
      });
      if (retorno) {
        Swal.fire("Sucesso", "Criado com sucesso", "success").then((result) => {
          if (result.isConfirmed) {
            voltar();
          }
        });
      }
    }

    fetchGetPerguntaOpcaos();
  };

  const voltar = () => {
    router.push(`/pergunta/${pergunta}`);
  };

  const formControlProps = {
    fullWidth: true,
  };

  return (
    <form onSubmit={handleSubmit}>
      <Card>
        <CardHeader color="info">
          <h4>Nova Opção!!</h4>
          <p>Nova Opção</p>
        </CardHeader>
        <CardBody>
          <GridContainer>
            <GridItem xs={12} sm={12} md={12}>
              <CustomInput
                labelText="Descrição"
                id="opcao"
                type="text"
                value={opcao}
                formControlProps={formControlProps}
                onChange={(event) => setOpcao(event.target.value)}
                error={false}
                maxlength="200"
                required
              />
            </GridItem>
          </GridContainer>
        </CardBody>
        <CardFooter>
          <Button onClick={voltar}>Cancelar</Button>
          <Button color="success" type="submit">
            Salvar
          </Button>
        </CardFooter>
      </Card>
    </form>
  );
};

NovoPerguntaOpcao.layout = Admin;
export default NovoPerguntaOpcao;
