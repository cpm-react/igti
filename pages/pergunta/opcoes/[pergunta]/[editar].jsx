import { useState, useEffect } from "react";
import { useRouter } from "next/router";

import Swal from "sweetalert2";

import Admin from "layouts/Admin.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import CardHeader from "components/Card/CardHeader.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import CustomSelect from "../../../../components/CustomSelect/CustomSelect";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";

import {
  PutPerguntaOpcao,
  GetPerguntaOpcaoPorId,
  DeletePerguntaOpcao,
} from "../../../../services/PerguntaOpcao";

const NovoPerguntaOpcao = () => {
  const router = useRouter();
  const { pergunta, editar } = router.query;

  const [id, setId] = useState([]);
  const [opcao, setOpcao] = useState([]);
  // Ready
  useEffect(() => {
    async function fetchGetUsuarios() {
      const retorno = await GetPerguntaOpcaoPorId(editar);
      if (retorno) {
        setId(editar);
        setOpcao(retorno.opcao);
      }
    }

    fetchGetUsuarios();
  }, []);

  const handleSubmit = (event) => {
    event.preventDefault();

    async function fetchGetPerguntaOpcaos() {
      const retorno = await PutPerguntaOpcao(editar, {
        id: editar,
        opcao,
        pergunta,
      });
      if (retorno) {
        Swal.fire("Sucesso", "Atualizado com sucesso", "success").then(
          (result) => {
            if (result.isConfirmed) {
              voltar();
            }
          }
        );
      }
    }

    fetchGetPerguntaOpcaos();
  };

  const formControlProps = {
    fullWidth: true,
  };

  const voltar = () => {
    router.push(`/pergunta/${pergunta}`);
  };

  const excluir = () => {
    Swal.fire({
      title: "Deseja excluir esse PerguntaOpcao??",
      showDenyButton: true,
      confirmButtonText: `Sim`,
      denyButtonText: `Não`,
    }).then((result) => {
      if (result.isConfirmed) {
        DeletePerguntaOpcao(editar);
        voltar();
      }
    });
  };

  return (
    <form onSubmit={handleSubmit}>
      <Card>
        <CardHeader color="info">
          <h4>Editar Opção!!</h4>
          <p>Editar Opção</p>
        </CardHeader>
        <CardBody>
          <GridContainer>
            <GridItem xs={12} sm={12} md={12}>
              <CustomInput
                labelText="Id"
                id="Id"
                type="text"
                value={id}
                formControlProps={formControlProps}
                error={false}
                required
                disabled
              />
            </GridItem>
          </GridContainer>
          <GridContainer>
            <GridItem xs={12} sm={12} md={12}>
              <CustomInput
                labelText="Opção"
                id="opcao"
                type="text"
                value={opcao}
                formControlProps={formControlProps}
                onChange={(event) => setOpcao(event.target.value)}
                error={false}
                maxlength="200"
                required
              />
            </GridItem>
          </GridContainer>
        </CardBody>
        <CardFooter>
          <Button onClick={voltar}>Cancelar</Button>
          <Button color="danger" onClick={excluir}>
            Excluir
          </Button>
          <Button color="success" type="submit">
            Salvar
          </Button>
        </CardFooter>
      </Card>
    </form>
  );
};

NovoPerguntaOpcao.layout = Admin;
export default NovoPerguntaOpcao;
