import { useEffect } from "react";
import { useState } from "react";
import { useRouter } from "next/router";

import Swal from "sweetalert2";

import Admin from "layouts/Admin.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import CardHeader from "components/Card/CardHeader.js";
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import CustomSelect from "../../components/CustomSelect/CustomSelect";

import { PostPergunta } from "../../services/Pergunta";
import { GetSessao } from "../../services/Usuario";
import { TipoPergunta } from "../enum";

const NovoPergunta = () => {
  // Ready
  useEffect(() => {
    GetSessao();
  }, []);

  const router = useRouter();

  const [descricao, setDescricao] = useState([]);
  const [tipoPergunta, setTipoPergunta] = useState([]);

  const handleSubmit = (event) => {
    event.preventDefault();

    async function fetchPerguntas() {
      const retorno = await PostPergunta({
        descricao,
        tipo_pergunta: tipoPergunta,
      });
      Swal.fire("Sucesso", "Criada com sucesso", "success").then((result) => {
        if (result.isConfirmed) {
          router.push(`/pergunta/${retorno.id}`);
        }
      });
    }

    fetchPerguntas();
  };

  const formControlProps = {
    fullWidth: true,
  };

  return (
    <form onSubmit={handleSubmit}>
      <Card>
        <CardHeader color="info">
          <h4>Nova Pergunta!!</h4>
          <p>Perguntas para os formulários</p>
        </CardHeader>
        <CardBody>
          <GridContainer>
            <GridItem xs={12} sm={12} md={8}>
              <CustomInput
                labelText="Descrição"
                id="descricao"
                type="text"
                value={descricao}
                formControlProps={formControlProps}
                onChange={(event) => setDescricao(event.target.value)}
                error={false}
                maxlength="90"
                required
              />
            </GridItem>
            <GridItem xs={12} sm={12} md={4}>
              <CustomSelect
                labelText="Tipo de pergunta"
                id="tipo_pergunta"
                value={tipoPergunta}
                currencies={TipoPergunta()}
                formControlProps={formControlProps}
                onChange={(event) => setTipoPergunta(event.target.value)}
                error={false}
                maxlength="1"
                required
              />
            </GridItem>
          </GridContainer>
        </CardBody>
        <CardFooter>
          <Button
            onClick={() => {
              router.push("/pergunta");
            }}
          >
            Cancelar
          </Button>
          <Button color="success" type="submit">
            Salvar
          </Button>
        </CardFooter>
      </Card>
    </form>
  );
};

NovoPergunta.layout = Admin;
export default NovoPergunta;
