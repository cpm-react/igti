import { useEffect, useState } from "react";
import { useRouter } from "next/router";

import Admin from "layouts/Admin.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import CardHeader from "components/Card/CardHeader.js";
import Table from "components/Table/Table.js";

import { GetPerguntas } from "../../services/Pergunta";
import { TipoPerguntaFiltroLabel } from "../enum";

const Pergunta = () => {
  const [perguntas, setPerguntas] = useState([]);
  const router = useRouter();

  useEffect(() => {
    async function fetchGetPerguntas() {
      const retorno = await GetPerguntas();

      if (retorno) {
        const result = retorno.results.map((pergunta) => [
          pergunta.id,
          TipoPerguntaFiltroLabel(pergunta.tipo_pergunta),
          pergunta.descricao,
        ]);

        setPerguntas(result);
      }
    }

    fetchGetPerguntas();
  }, []);

  const novoPergunta = () => {
    router.push("/pergunta/novo");
  };

  return (
    <Card>
      <CardHeader color="info">
        <h4>Pergunta!!</h4>
        <p>Perguntas para os formulários</p>
      </CardHeader>
      <CardBody>
        <Table
          tableHeaderColor="info"
          tableHead={["ID", "Tipo", "Pergunta"]}
          tableData={perguntas}
          linkUrl="/pergunta"
        />
      </CardBody>
      <CardFooter>
        <Button onClick={novoPergunta}>Novo</Button>
      </CardFooter>
    </Card>
  );
};

Pergunta.layout = Admin;
export default Pergunta;
