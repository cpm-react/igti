import React from "react";
import Card from "components/Card/Card.js";
import { makeStyles } from "@material-ui/core/styles";
import Router from "next/router";

const importante = " !important";

const styles = {
  card: {
    height: `187px${importante}`,
    left: `50%${importante}`,
    marginLeft: `-250px${importante}`,
    marginTop: `-93.5px${importante}`,
    paddingBottom: "40px",
    position: `absolute${importante}`,
    textAlign: "center",
    top: `50%${importante}`,
    width: `500px${importante}`,
  },
  link: {
    color: "rgba(0, 0, 0, 0.87)",
    cursor: "pointer",
  },
};

export default function Custom404() {
  const useStyles = makeStyles(styles);
  const classes = useStyles();

  return (
    <Card className={classes.card}>
      <h1>404</h1>
      <h3>Página não encontrada</h3>

      <a className={classes.link} onClick={() => Router.back()}>Voltar...</a>
    </Card>
  );
}
