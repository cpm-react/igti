const TipoPessoa = () => {
  return [
    {
      value: "F",
      label: "Fisica",
    },
    {
      value: "J",
      label: "Júridica",
    },
  ];
};

const TipoPessoaFiltro = (valor) => {
  return TipoPessoa().filter(function (value) {
    return value.value == valor;
  });
};

const TipoPessoaFiltroLabel = (valor) => {
  return TipoPessoaFiltro(valor)[0].label;
};

const Estado = () => {
  return [
    { value: "AC", label: "Acre" },
    { value: "AL", label: "Alagoas" },
    { value: "AP", label: "Amapá" },
    { value: "AM", label: "Amazonas" },
    { value: "BA", label: "Bahia" },
    { value: "CE", label: "Ceará" },
    { value: "DF", label: "Distrito Federal" },
    { value: "ES", label: "Espírito Santo" },
    { value: "GO", label: "Goiás" },
    { value: "MA", label: "Maranhão" },
    { value: "MT", label: "Mato Grosso" },
    { value: "MS", label: "Mato Grosso do Sul" },
    { value: "MG", label: "Minas Gerais" },
    { value: "PA", label: "Pará" },
    { value: "PB", label: "Paraíba" },
    { value: "PR", label: "Paraná" },
    { value: "PE", label: "Pernambuco" },
    { value: "PI", label: "Piauí" },
    { value: "RJ", label: "Rio de Janeiro" },
    { value: "RN", label: "Rio Grande do Norte" },
    { value: "RS", label: "Rio Grande do Sul" },
    { value: "RO", label: "Rondônia" },
    { value: "RR", label: "Roraima" },
    { value: "SC", label: "Santa Catarina" },
    { value: "SP", label: "São Paulo" },
    { value: "SE", label: "Sergipe" },
    { value: "TO", label: "Tocantins" },
  ];
};

const EstadoFiltro = (valor) => {
  return Estado().filter(function (value) {
    return value.value == valor;
  });
};

const EstadoFiltroLabel = (valor) => {
  return EstadoFiltro(valor)[0].label;
};

const TipoContato = () => {
  return [
    { value: "T", label: "Telefone" },
    { value: "E", label: "E-mail" },
    { value: "R", label: "Rede Social" },
  ];
};

const TipoContatoFiltro = (valor) => {
  return TipoContato().filter(function (value) {
    return value.value == valor;
  });
};

const TipoContatoFiltroLabel = (valor) => {
  return TipoContatoFiltro(valor)[0].label;
};

const AtedimentoStatus = () => {
  return [
    { value: "AGENDADO", label: "Agendado" },
    { value: "ATENDENDO", label: "Atendendo" },
    { value: "ATENDIDO", label: "Atendido" },
    { value: "CANCELADO", label: "Cancelado" },
  ];
};

const AtedimentoStatusFiltro = (valor) => {
  return AtedimentoStatus().filter(function (value) {
    return value.value == valor;
  });
};

const AtedimentoStatusFiltroLabel = (valor) => {
  return AtedimentoStatusFiltro(valor)[0].label;
};

const TipoPergunta = () => {
  return [
    { label: "Cor", value: "COLOR" },
    { label: "Data", value: "DATE" },
    { label: "E-mail", value: "EMAIL" },
    { label: "Faixa", value: "RANGE" },
    { label: "Hora", value: "TIME" },
    { label: "Múltipla escolha", value: "CHECKBOX" },
    { label: "Número", value: "NUMBER" },
    { label: "Rádio", value: "RADIO" },
    { label: "Texto", value: "TEXT" },
  ];
};

const TipoPerguntaFiltro = (valor) => {
  return TipoPergunta().filter(function (value) {
    return value.value == valor;
  });
};

const TipoPerguntaFiltroLabel = (valor) => {
  return TipoPerguntaFiltro(valor)[0].label;
};

const DateOption = () => {
  return {
    year: "numeric",
    month: "long",
    weekday: "long",
    day: "numeric",
    hour: "numeric",
    minute: "numeric",
    second: "numeric",
    timeZoneName: "long",
  };
};

export {
  AtedimentoStatus,
  AtedimentoStatusFiltro,
  AtedimentoStatusFiltroLabel,
  DateOption,
  Estado,
  EstadoFiltro,
  EstadoFiltroLabel,
  TipoContato,
  TipoContatoFiltro,
  TipoContatoFiltroLabel,
  TipoPergunta,
  TipoPerguntaFiltro,
  TipoPerguntaFiltroLabel,
  TipoPessoa,
  TipoPessoaFiltro,
  TipoPessoaFiltroLabel,
};
