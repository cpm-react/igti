import React from "react";
import Router from "next/router";

// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import ListItem from "@material-ui/core/ListItem";
import List from "@material-ui/core/List";
// core components
import styles from "assets/jss/nextjs-material-dashboard/components/footerStyle.js";

import { GetSessao, Sair } from "../../services/Usuario";

const Footer = (props) => {
  const useStyles = makeStyles(styles);
  const classes = useStyles();

  const refresh = (event) => {
    GetSessao();
    Router.reload();
  };

  const sair = (event) => {
    Sair();
  };

  return (
    <footer className={classes.footer}>
      <div className={classes.container}>
        <div className={classes.left}>
          <List className={classes.list}>
            <ListItem className={classes.inlineBlock}>
              <a
                href="https://www.igti.com.br/"
                target="_blank"
                className={classes.block}
              >
                IGTI
              </a>
            </ListItem>
            <ListItem className={classes.inlineBlock}>
              <a href="#" className={classes.block} onClick={(e) => refresh()}>
                REFRESH
              </a>
            </ListItem>
            <ListItem className={classes.inlineBlock}>
              <a href="#" className={classes.block} onClick={(e) => sair()}>
                SAIR
              </a>
            </ListItem>
          </List>
        </div>
        <p className={classes.right}>
          <span>
            &copy; {1900 + new Date().getYear()}{" "}
            <a href="/" className={classes.a}>
              {props.titulo}
            </a>
          </span>
        </p>
      </div>
    </footer>
  );
};

export { Footer };
