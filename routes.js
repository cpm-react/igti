import {
  CalendarToday,
  CardMembership,
  Forum,
  Gavel,
  LocalHospital,
  People,
  PeopleAlt,
  Person,
  PersonSharp,
  QuestionAnswer,
} from "@material-ui/icons";

const dashboardRoutes = [
  { path: "/agenda", name: "Agenda", icon: CalendarToday },
  {
    collapse: true,
    name: "Pessoas",
    views: [
      { path: "/cliente", name: "Cliente", icon: Person },
      { path: "/profissional", name: "Profissional", icon: PersonSharp },
      { path: "/usuario", name: "Usuário", icon: People },
    ],
  },
  {
    collapse: true,
    name: "Básicos",
    views: [
      { path: "/cid", name: "CID", icon: LocalHospital },
      { path: "/formulario", name: "Formulário", icon: Forum },
      { path: "/orgao", name: "Órgão", icon: PeopleAlt },
      { path: "/papel", name: "Papel", icon: Gavel },
      { path: "/pergunta", name: "Pergunta", icon: QuestionAnswer },
      { path: "/plano", name: "Plano", icon: CardMembership },
    ],
  },
];

export default dashboardRoutes;
