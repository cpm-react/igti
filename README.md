# Controle Digital

O [React](https://pt-br.reactjs.org/) é uma biblioteca JavaScript de código aberto com foco em criar interfaces de usuário em páginas web. É mantido pelo Facebook, Instagram, outras empresas e uma comunidade de desenvolvedores individuais. É utilizado nos sites da Netflix, Imgur, Feedly, Airbnb, SeatGeek, HelloSign, Walmart e outros.

## Componentes

- [`Next.js`](https://nextjs.org/)
- [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app)
- [`Template`](https://www.creative-tim.com/product/nextjs-material-dashboard)
- [`Ícones`](https://fonts.google.com/icons?selected=Material+Icons)
- [`sweetalert2`](https://sweetalert2.github.io/)
- [`react-big-calendar`](https://github.com/jquense/react-big-calendar)
- [`Vercel`](https://vercel.com/)

## Instalando
- [Node](https://gitlab.com/cpm-react/igti/-/wikis/Instalando-Node)
## Iniciando

Primeiro, rode o servidor com o seguinte comando:

```ssh
npm run dev
```

Abra [http://localhost:3000](http://localhost:3000) no seu browser para ver o resultado.

## Demais projetos

- [Projeto `FE`](https://gitlab.com/cpm-python/controle-digital)