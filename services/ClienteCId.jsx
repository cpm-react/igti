import {
  ExecutaGet,
  ExecutaGetPorId,
  ExecutaPost,
  ExecutaPut,
  ExecutaDelete,
} from "./Sessao";

const url = "cliente_cid";

const GetUsuarioPapeis = async () => {
  const clientecids = await ExecutaGet(url);
  return clientecids;
};

const GetClienteCidPorId = async (id) => {
  const clientecids = await ExecutaGetPorId(url, id);
  return clientecids;
};

const PostClienteCid = async (clientecid) => {
  console.log(clientecid);

  const retorno = await ExecutaPost(url, clientecid);
  return retorno;
};

const PutClienteCid = async (id, clientecid) => {
  const retorno = await ExecutaPut(url, id, clientecid);
  return retorno;
};

const DeleteClienteCid = async (id) => {
  const retorno = await ExecutaDelete(url, id);
  return retorno;
};

export {
  GetUsuarioPapeis,
  GetClienteCidPorId,
  PostClienteCid,
  PutClienteCid,
  DeleteClienteCid,
};
