import {
  ExecutaGet,
  ExecutaGetPorId,
  ExecutaPost,
  ExecutaPut,
  ExecutaDelete,
} from "./Sessao";

const url = "formulario";

const GetFormularios = async () => {
  return await ExecutaGet(url);
};

const GetFormularioPorId = async (id) => {
  return await ExecutaGetPorId(url, id);
};

const PostFormulario = async (formulario) => {
  return await ExecutaPost(url, formulario);
};

const PutFormulario = async (id, formulario) => {
  return await ExecutaPut(url, id, formulario);
};

const DeleteFormulario = async (id) => {
  return await ExecutaDelete(url, id);
};

export {
  DeleteFormulario,
  GetFormularioPorId,
  GetFormularios,
  PostFormulario,
  PutFormulario,
};
