import {
  ExecutaGet,
  ExecutaGetPorId,
  ExecutaPost,
  ExecutaPut,
  ExecutaDelete,
} from "./Sessao";

const GetEnderecos = async () => {
  return await ExecutaGet("endereco");
};

const GetEnderecoPorId = async (id) => {
  return await ExecutaGetPorId("endereco", id);
};

const PostEndereco = async (endereco) => {
  return await ExecutaPost("endereco", endereco);
};

const PutEndereco = async (id, endereco) => {
  return await ExecutaPut("endereco", id, endereco);
};

const DeleteEndereco = async (id) => {
  return await ExecutaDelete("endereco", id);
};

export { GetEnderecos, GetEnderecoPorId, PostEndereco, PutEndereco, DeleteEndereco };
