import {
  ExecutaDelete,
  ExecutaGet,
  ExecutaGetPorId,
  ExecutaPost,
  ExecutaPut,
  GetUsuario,
} from "./Sessao";

const url = "usuario_profissional";

const GetUsuarioProfissionais = async () => {
  return await ExecutaGet(url);
};

const GetUsuarioProfissionalPorId = async (id) => {
  return await ExecutaGetPorId(url, id);
};

const GetUsuarioProfissionalPorUsuarioId = async () => {
  const id = GetUsuario();
  let retorno = [];
  const lista = await GetUsuarioProfissionais();
  if (lista) {
    lista.results.map((item) => {
      if (item.usuario == id) {
        retorno.push(item.profissional);
      }
    });
  }

  return { results: retorno };
};

const PostUsuarioProfissional = async (usuarioprofissional) => {
  return await ExecutaPost(url, usuarioprofissional);
};

const PutUsuarioProfissional = async (id, usuarioprofissional) => {
  return await ExecutaPut(url, id, usuarioprofissional);
};

const DeleteUsuarioProfissional = async (id) => {
  return await ExecutaDelete(url, id);
};

export {
  DeleteUsuarioProfissional,
  GetUsuarioProfissionais,
  GetUsuarioProfissionalPorId,
  GetUsuarioProfissionalPorUsuarioId,
  PostUsuarioProfissional,
  PutUsuarioProfissional,
};
