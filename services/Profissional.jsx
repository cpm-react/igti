import {
  ExecutaGet,
  ExecutaGetPorId,
  ExecutaPost,
  ExecutaPut,
  ExecutaDelete,
} from "./Sessao";

const GetProfissionals = async () => {
  return await ExecutaGet("profissional");
};

const GetProfissionalPorId = async (id) => {
  return await ExecutaGetPorId("profissional", id);
};

const PostProfissional = async (profissional) => {
  return await ExecutaPost("profissional", profissional);
};

const PutProfissional = async (id, profissional) => {
  return await ExecutaPut("profissional", id, profissional);
};

const DeleteProfissional = async (id) => {
  return await ExecutaDelete("profissional", id);
};

export {
  GetProfissionals,
  GetProfissionalPorId,
  PostProfissional,
  PutProfissional,
  DeleteProfissional,
};
