import {
  ExecutaGet,
  ExecutaGetPorId,
  ExecutaPost,
  ExecutaPut,
  ExecutaDelete,
} from "./Sessao";

const url = "usuario_papel";

const GetUsuarioPapeis = async () => {
  const usuariopapels = await ExecutaGet(url);
  return usuariopapels;
};

const GetUsuarioPapelPorId = async (id) => {
  const usuariopapels = await ExecutaGetPorId(url, id);
  return usuariopapels;
};

const PostUsuarioPapel = async (usuariopapel) => {
  console.log(usuariopapel)
  
  const retorno = await ExecutaPost(url, usuariopapel);
  return retorno;
};

const PutUsuarioPapel = async (id, usuariopapel) => {
  const retorno = await ExecutaPut(url, id, usuariopapel);
  return retorno;
};

const DeleteUsuarioPapel = async (id) => {
  const retorno = await ExecutaDelete(url, id);
  return retorno;
};

export { GetUsuarioPapeis, GetUsuarioPapelPorId, PostUsuarioPapel, PutUsuarioPapel, DeleteUsuarioPapel };
