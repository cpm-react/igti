import {
  ExecutaGet,
  ExecutaGetPorId,
  ExecutaPost,
  ExecutaPut,
  ExecutaDelete,
} from "./Sessao";

const url = "formulario_papel";

const GetFormularioPapeis = async () => {
  return await ExecutaGet(url);
};

const GetFormularioPapelPorId = async (id) => {
  return await ExecutaGetPorId(url, id);
};

const PostFormularioPapel = async (formulariopapel) => {
  return await ExecutaPost(url, formulariopapel);
};

const PutFormularioPapel = async (id, formulariopapel) => {
  return await ExecutaPut(url, id, formulariopapel);
};

const DeleteFormularioPapel = async (id) => {
  return await ExecutaDelete(url, id);
};

export {
  GetFormularioPapeis,
  GetFormularioPapelPorId,
  PostFormularioPapel,
  PutFormularioPapel,
  DeleteFormularioPapel,
};
