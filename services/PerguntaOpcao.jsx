import {
  ExecutaGet,
  ExecutaGetPorId,
  ExecutaPost,
  ExecutaPut,
  ExecutaDelete,
} from "./Sessao";

const url = "pergunta_opcao";

const GetPerguntaOpcoes = async () => {
  return await ExecutaGet(url);
};

const GetPerguntaOpcaoPorId = async (id) => {
  return await ExecutaGetPorId(url, id);
};

const PostPerguntaOpcao = async (perguntaopcao) => {
  return await ExecutaPost(url, perguntaopcao);
};

const PutPerguntaOpcao = async (id, perguntaopcao) => {
  return await ExecutaPut(url, id, perguntaopcao);
};

const DeletePerguntaOpcao = async (id) => {
  return await ExecutaDelete(url, id);
};

export {
  GetPerguntaOpcoes,
  GetPerguntaOpcaoPorId,
  PostPerguntaOpcao,
  PutPerguntaOpcao,
  DeletePerguntaOpcao,
};
