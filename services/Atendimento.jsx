import {
  ExecutaDelete,
  ExecutaGet,
  ExecutaGetPorId,
  ExecutaPost,
  ExecutaPut,
} from "./Sessao";

const service = "atendimento";

const GetAtendimentos = async () => {
  return await ExecutaGet(service);
};

const GetAtendimentoPorId = async (id) => {
  return await ExecutaGetPorId(service, id);
};

const GetAtendimentoPorProfissional = async (id) => {
  return { results: await ExecutaGetPorId(service, `profissional/${id}`) };
};

const PostAtendimento = async (atendimento) => {
  return await ExecutaPost(service, atendimento);
};

const PutAtendimento = async (id, atendimento) => {
  return await ExecutaPut(service, id, atendimento);
};

const DeleteAtendimento = async (id) => {
  return await ExecutaDelete(service, id);
};

export {
  DeleteAtendimento,
  GetAtendimentoPorId,
  GetAtendimentoPorProfissional,
  GetAtendimentos,
  PostAtendimento,
  PutAtendimento,
};
