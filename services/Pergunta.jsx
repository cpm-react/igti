import {
  ExecutaGet,
  ExecutaGetPorId,
  ExecutaPost,
  ExecutaPut,
  ExecutaDelete,
} from "./Sessao";

const url = "pergunta";

const GetPerguntas = async () => {
  return await ExecutaGet(url);
};

const GetPerguntaPorId = async (id) => {
  return await ExecutaGetPorId(url, id);
};

const PostPergunta = async (pergunta) => {
  return await ExecutaPost(url, pergunta);
};

const PutPergunta = async (id, pergunta) => {
  return await ExecutaPut(url, id, pergunta);
};

const DeletePergunta = async (id) => {
  return await ExecutaDelete(url, id);
};

export {
  DeletePergunta,
  GetPerguntaPorId,
  GetPerguntas,
  PostPergunta,
  PutPergunta,
};
