import {
  ExecutaGet,
  ExecutaGetPorId,
  ExecutaPost,
  ExecutaPut,
  ExecutaDelete,
} from "./Sessao";

const GetCids = async () => {
  const cids = await ExecutaGet("cid");
  return cids;
};

const GetCidPorId = async (id) => {
  const cids = await ExecutaGetPorId("cid", id);
  console.log(cids);
  return cids;
};

const PostCid = async (cid) => {
  const retorno = await ExecutaPost("cid", cid);
  return retorno;
};

const PutCid = async (id, cid) => {
  const retorno = await ExecutaPut("cid", id, cid);
  return retorno;
};

const DeleteCid = async (id) => {
  const retorno = await ExecutaDelete("cid", id);
  return retorno;
};

export { GetCids, GetCidPorId, PostCid, PutCid, DeleteCid };
