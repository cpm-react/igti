import Router from "next/router";
import {
  ExecutaDelete,
  ExecutaGet,
  ExecutaGetPorId,
  ExecutaPost,
  ExecutaPut,
  SalvaSessao,
} from "./Sessao";

const GetSessao = async () => {
  const dados = await ExecutaGet("sessao");
  if (dados) {
    SalvaSessao(dados.token, dados.cliente, dados.fim);
  } else {
    Sair();
  }
};

const GetClientes = async () => {
  return await ExecutaGet("cliente");
};

const GetClientePorId = async (id) => {
  return await ExecutaGetPorId("cliente", id);
};

const PostCliente = async (cliente) => {
  return await ExecutaPost("cliente", cliente);
};

const PutCliente = async (id, cliente) => {
  return await ExecutaPut("cliente", id, cliente);
};

const DeleteCliente = async (id) => {
  return await ExecutaDelete("cliente", id);
};

export {
  DeleteCliente,
  GetClientePorId,
  GetClientes,
  GetSessao,
  PostCliente,
  PutCliente,
};
