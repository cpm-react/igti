import { convertToObject } from "typescript";
import {
  ExecutaDelete,
  ExecutaGet,
  ExecutaGetPorId,
  ExecutaPost,
  ExecutaPut,
} from "./Sessao";

const service = "evolucao_resposta";

const GetEvolucaoRespostas = async () => {
  return await ExecutaGet(service);
};

const GetEvolucaoRespostaPorId = async (id) => {
  return await ExecutaGetPorId(service, id);
};

const PostEvolucaoResposta = async (obj) => {
  if (
    obj &&
    obj.resposta_texto &&
    obj.resposta_texto != "" &&
    obj.resposta_texto != "<span>​</span>"
  )
    return await ExecutaPost(service, obj);
};

const PutEvolucaoResposta = async (id, obj) => {
  return await ExecutaPut(service, id, obj);
};

const DeleteEvolucaoResposta = async (id) => {
  return await ExecutaDelete(service, id);
};

export {
  DeleteEvolucaoResposta,
  GetEvolucaoRespostaPorId,
  GetEvolucaoRespostas,
  PostEvolucaoResposta,
  PutEvolucaoResposta,
};
