import {
  ExecutaDelete,
  ExecutaGet,
  ExecutaGetPorId,
  ExecutaPost,
  ExecutaPut,
} from "./Sessao";

const service = "evolucao";

const GetEvolucoes = async () => {
  return await ExecutaGet(service);
};

const GetEvolucaoPorId = async (id) => {
  return await ExecutaGetPorId(service, id);
};

const PostEvolucao = async (evolucao) => {
  return await ExecutaPost(service, evolucao);
};

const PutEvolucao = async (id, evolucao) => {
  return await ExecutaPut(service, id, evolucao);
};

const DeleteEvolucao = async (id) => {
  return await ExecutaDelete(service, id);
};

export {
  DeleteEvolucao,
  GetEvolucaoPorId,
  GetEvolucoes,
  PostEvolucao,
  PutEvolucao,
};
