import {
  ExecutaGet,
  ExecutaGetPorId,
  ExecutaPost,
  ExecutaPut,
  ExecutaDelete,
} from "./Sessao";

const GetPapels = async () => {
  const papeis = await ExecutaGet("papel");
  return papeis;
};

const GetPapelPorId = async (id) => {
  const papeis = await ExecutaGetPorId("papel", id);
  return papeis;
};

const PostPapel = async (papel) => {
  const retorno = await ExecutaPost("papel", papel);
  return retorno;
};

const PutPapel = async (id, papel) => {
  const retorno = await ExecutaPut("papel", id, papel);
  return retorno;
};

const DeletePapel = async (id) => {
  const retorno = await ExecutaDelete("papel", id);
  return retorno;
};

export { GetPapels, GetPapelPorId, PostPapel, PutPapel, DeletePapel };
