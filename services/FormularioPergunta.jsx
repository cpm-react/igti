import {
  ExecutaGet,
  ExecutaGetPorId,
  ExecutaPost,
  ExecutaPut,
  ExecutaDelete,
} from "./Sessao";

const url = "formulario_pergunta";

const GetFormularioPerguntas = async () => {
  return await ExecutaGet(url);
};

const GetFormularioPerguntaPorId = async (id) => {
  return await ExecutaGetPorId(url, id);
};

const PostFormularioPergunta = async (formulariopergunta) => {
  return await ExecutaPost(url, formulariopergunta);
};

const PutFormularioPergunta = async (id, formulariopergunta) => {
  return await ExecutaPut(url, id, formulariopergunta);
};

const DeleteFormularioPergunta = async (id) => {
  return await ExecutaDelete(url, id);
};

export {
  GetFormularioPerguntas,
  GetFormularioPerguntaPorId,
  PostFormularioPergunta,
  PutFormularioPergunta,
  DeleteFormularioPergunta,
};
