import Router from "next/router";
import {
  ExecutaDelete,
  ExecutaGet,
  ExecutaGetPorId,
  ExecutaPost,
  ExecutaPut,
  GetHeaders,
  GetToken,
  GetUrl,
  LimpaStorage,
  SalvaSessao,
} from "./Sessao";

import Swal from "sweetalert2";

const Logar = (usuario) => {
  fetch(`${GetUrl()}/api/v2/sessao/login/`, {
    method: "POST",
    headers: GetHeaders(),
    body: JSON.stringify(usuario),
  })
    .then((resposta) => {
      if (resposta.ok) {
        return resposta.json();
      }

      Swal.fire("Erro", "Usuário e/ou senha inválidos", "error");
      return { token: null };
    })
    .then(function (dados) {
      if (dados.token != null) {
        SalvaSessao(dados.token, dados.usuario, dados.fim);
        Router.push("/");
      }
    });
};

const FinalizaSair = () => {
  LimpaStorage();

  Router.push("/login");

  return null;
};

const Sair = () => {
  console.log(1);
  if (!GetToken() || GetToken() == null || GetToken() == "") {
    console.log(2);
    FinalizaSair();
    console.log(3);
  } else {
    console.log(GetToken());

    fetch(`${GetUrl()}/api/v2/sessao/logout/${GetToken()}/`, {
      method: "PUT",
      headers: GetHeaders(),
    }).then((resposta) => {
      console.log(5);
      FinalizaSair();
    });
    console.log(6);
  }
};

const GetSessao = async () => {
  const dados = await ExecutaGet("sessao");
  if (dados) {
    SalvaSessao(dados.token, dados.usuario, dados.fim);
  } else {
    Sair();
  }
};

const GetUsuarios = async () => {
  return await ExecutaGet("usuario");
};

const GetUsuarioPorId = async (id) => {
  return await ExecutaGetPorId("usuario", id);
};

const PostUsuario = async (usuario) => {
  return await ExecutaPost("usuario", usuario);
};

const PutUsuario = async (id, usuario) => {
  return await ExecutaPut("usuario", id, usuario);
};

const DeleteUsuario = async (id) => {
  return await ExecutaDelete("usuario", id);
};

export {
  DeleteUsuario,
  GetSessao,
  GetUsuarioPorId,
  GetUsuarios,
  Logar,
  PostUsuario,
  PutUsuario,
  Sair,
};
