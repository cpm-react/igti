import Router from "next/router";
import Swal from "sweetalert2";

import { Sair, GetUsuarioPorId } from "./Usuario";

const name = "@controle-digital/";

const name_token = `${name}token`;
const name_usuario = `${name}usuario`;
const name_usuario_completo = `${name}usuario_completo`;
const name_fim_sessao = `${name}fim_sessao`;

const GetToken = () => {
  return localStorage.getItem(name_token);
};

const SetToken = (token) => {
  localStorage.setItem(name_token, token);
};

const RemoveToken = () => {
  localStorage.removeItem(name_token);
};

const GetUsuario = () => {
  return localStorage.getItem(name_usuario);
};

const GetUsuarioCompleto = () => {
  const valor_string = localStorage.getItem(name_usuario_completo);

  if (!valor_string) return null;

  return JSON.parse(valor_string);
};

const GetProfissionalDoUsuario = () => {
  const retorno = GetUsuarioCompleto();

  if (!retorno) {
    Sair();
    return null;
  }

  return retorno.usuarios_profissionais[0].profissional.id;
};

const SetUsuario = async (usuario_id) => {
  localStorage.setItem(name_usuario, usuario_id);
  const usuarioCompleto = await GetUsuarioPorId(usuario_id);
  if (usuarioCompleto) {
    localStorage.setItem(
      name_usuario_completo,
      JSON.stringify(usuarioCompleto)
    );
  }
};

const RemoveUsuario = () => {
  localStorage.removeItem(name_usuario);
  localStorage.removeItem(name_usuario_completo);
};

const GetFimSessao = () => {
  return localStorage.getItem(name_fim_sessao);
};

const SetFimSessao = (token) => {
  localStorage.setItem(name_fim_sessao, token);
};

const RemoveFimSessao = () => {
  localStorage.removeItem(name_fim_sessao);
};

const LimpaStorage = () => {
  RemoveToken();
  RemoveUsuario();
  RemoveFimSessao();
};

const SalvaSessao = (token, usuario, fim_sessao) => {
  SetToken(token);
  SetUsuario(usuario);
  SetFimSessao(fim_sessao);
};

const GetUrl = () => {
  return "http://127.0.0.1:8000";
};

const GetFullUrl = (url) => {
  if (GetToken() == null) {
    Sair();
    return null;
  }

  return `${GetUrl()}/${GetVersionUrn()}/${url}/${GetToken()}/`;
};

const GetVersionUrn = () => {
  return "api/v2";
};

const GetHeaders = () => {
  return {
    "Content-Type": "application/json",
    Authorization: "Token 1f80d64f2627b08aba5474ec113335062de05b4f",
  };
};

const ValidaResposta = (resposta) => {
  if (resposta.ok) {
    return resposta.json();
  }

  if (resposta.status == 401) {
    Sair();
    return null;
  }

  let mensagem = resposta.statusText;

  if (resposta.status == 400) {
    Swal.fire("Atenção", "Valide todos os campos obrigatórios", "warning").then(
      (result) => {
        if (result.isConfirmed) {
          return null;
        }
      }
    );
    return null;
  }

  if (resposta.status == 404) {
    mensagem = "Não encontrado";
  }

  Swal.fire("Erro", mensagem, "error").then((result) => {
    if (result.isConfirmed) {
      Router.back();
    }
  });
  return null;
};

const ExecutaGet = async (url) => {
  const retorno = await fetch(`${GetFullUrl(url)}`, {
    method: "GET",
    headers: GetHeaders(),
  })
    .then((resposta) => {
      return ValidaResposta(resposta);
    })
    .then(function (dados) {
      if (dados) {
        return dados;
      }
    });

  return retorno;
};

const ExecutaGetPorId = async (url, id) => {
  const retorno = await fetch(`${GetFullUrl(url)}${id}/`, {
    method: "GET",
    headers: GetHeaders(),
  })
    .then((resposta) => {
      return ValidaResposta(resposta);
    })
    .then(function (dados) {
      if (dados) {
        return dados;
      }
    });

  return retorno;
};

const ExecutaPost = async (url, body) => {
  const retorno = fetch(`${GetFullUrl(url)}`, {
    method: "POST",
    headers: GetHeaders(),
    body: JSON.stringify(body),
  })
    .then((resposta) => {
      return ValidaResposta(resposta);
    })
    .then(function (dados) {
      if (dados) {
        return dados;
      }
    });

  return retorno;
};

const ExecutaPut = async (url, id, body) => {
  const retorno = await fetch(`${GetFullUrl(url)}${id}/`, {
    method: "PUT",
    headers: GetHeaders(),
    body: JSON.stringify(body),
  })
    .then((resposta) => {
      return ValidaResposta(resposta);
    })
    .then(function (dados) {
      if (dados) {
        return dados;
      }
    });

  return retorno;
};

const ExecutaDelete = async (url, id) => {
  const retorno = await fetch(`${GetFullUrl(url)}${id}/`, {
    method: "DELETE",
    headers: GetHeaders(),
  })
    .then((resposta) => {
      return ValidaResposta(resposta);
    })
    .then(function (dados) {
      if (dados) {
        return dados;
      }
    });

  return retorno;
};

export {
  ExecutaDelete,
  ExecutaGet,
  ExecutaGetPorId,
  ExecutaPost,
  ExecutaPut,
  GetFimSessao,
  GetHeaders,
  GetProfissionalDoUsuario,
  GetToken,
  GetUrl,
  GetUsuario,
  GetUsuarioCompleto,
  LimpaStorage,
  RemoveFimSessao,
  RemoveToken,
  RemoveUsuario,
  SalvaSessao,
  SetFimSessao,
  SetToken,
  SetUsuario,
};
