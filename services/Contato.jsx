import {
  ExecutaGet,
  ExecutaGetPorId,
  ExecutaPost,
  ExecutaPut,
  ExecutaDelete,
} from "./Sessao";

const GetContatos = async () => {
  return await ExecutaGet("contato");
};

const GetContatoPorId = async (id) => {
  return await ExecutaGetPorId("contato", id);
};

const PostContato = async (contato) => {
  return await ExecutaPost("contato", contato);
};

const PutContato = async (id, contato) => {
  return await ExecutaPut("contato", id, contato);
};

const DeleteContato = async (id) => {
  return await ExecutaDelete("contato", id);
};

export { GetContatos, GetContatoPorId, PostContato, PutContato, DeleteContato };
