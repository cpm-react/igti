import {
  ExecutaGet,
  ExecutaGetPorId,
  ExecutaPost,
  ExecutaPut,
  ExecutaDelete,
} from "./Sessao";

const GetPlanos = async () => {
  const planos = await ExecutaGet("plano");
  return planos;
};

const GetPlanoPorId = async (id) => {
  const planos = await ExecutaGetPorId("plano", id);
  console.log(planos);
  return planos;
};

const PostPlano = async (plano) => {
  const retorno = await ExecutaPost("plano", plano);
  return retorno;
};

const PutPlano = async (id, plano) => {
  const retorno = await ExecutaPut("plano", id, plano);
  return retorno;
};

const DeletePlano = async (id) => {
  const retorno = await ExecutaDelete("plano", id);
  return retorno;
};

export { GetPlanos, GetPlanoPorId, PostPlano, PutPlano, DeletePlano };
