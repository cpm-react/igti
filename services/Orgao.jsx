import {
  ExecutaGet,
  ExecutaGetPorId,
  ExecutaPost,
  ExecutaPut,
  ExecutaDelete,
} from "./Sessao";

const GetOrgaos = async () => {
  const orgaos = await ExecutaGet("orgao");
  return orgaos;
};

const GetOrgaoPorId = async (id) => {
  const orgaos = await ExecutaGetPorId("orgao", id);
  console.log(orgaos);
  return orgaos;
};

const PostOrgao = async (orgao) => {
  const retorno = await ExecutaPost("orgao", orgao);
  return retorno;
};

const PutOrgao = async (id, orgao) => {
  const retorno = await ExecutaPut("orgao", id, orgao);
  return retorno;
};

const DeleteOrgao = async (id) => {
  const retorno = await ExecutaDelete("orgao", id);
  return retorno;
};

export { GetOrgaos, GetOrgaoPorId, PostOrgao, PutOrgao, DeleteOrgao };
